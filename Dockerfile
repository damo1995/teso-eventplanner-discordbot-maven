FROM amazoncorretto:11
ARG JARFILE
WORKDIR /app
COPY $JARFILE "TESO-EventPLanner.jar"
ENV BODGEBOT_USEENV=1
CMD ["java", "-jar", "TESO-EventPLanner.jar"]