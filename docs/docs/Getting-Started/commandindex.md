
# Creating a new event

## Dungon
Used for creating new Dungeon run's
!!! note 
    Remember to sign-up for your role after creation!

`!dungeon <shortcode> <date> <time> <description>`

##### Arguments
* `<shortcode>` - dungeon shortcode from [dungeon list](/Instances/dungeons)  
* `<date>` - date for the event to take place in dd/mm/yyyy format  
* `<time>` - time for the event to take place in hh:mm 24 hour format  
* `<description>`(Optional) - an optional description to show with your event   

##### Example

`!dungeon vFG1 24/04/2021 17:00 Veteran FG1 run for Sticker's` 

Veteran Fungal Grotto I on the 24<sup>th</sup> April 2021 @ 17:00 with a description of "Veteran VG1 run for Sticker's"

## Pledges
Used for creating new Pledge run's
!!! note 
    Remember to sign-up for your role after creation!


* `!pledge <difficulty> <date> <time> <description>` 

##### Arguments
* `<difficulty>` - difficulty option  
    1. `normal` - Normal difficulty  
    2. `veteran` - Veteran difficulty  
    3. `veteranhm` - Veteran difficulty with Hard-Mode challange  

* `<date>` - date for the event to take place in dd/mm/yyyy format  
* `<time>` - time for the event to take place in hh:mm 24 hour format  
* `<description>`(Optional) - an optional description to show with your event   

##### Example

`!pledge normal 24/04/2021 17:00 Normal pledge run` 

Normal Pledge Run on the 24<sup>th</sup> April 2021 @ 17:00 with a description of "Normal pledge run"


## Trial
Used for creating new trial run's (Can only be used by administrators)
!!! note 
    Remember to sign-up for your role after creation!
* `!trial <shortcode> <date> <time> <description>` - Creates a new trial event

##### Arguments
* `<shortcode>` - trial shortcode from [trial list](/Instances/trials)  
* `<date>` - date for the event to take place in dd/mm/yyyy format  
* `<time>` - time for the event to take place in hh:mm 24 hour format  
* `<description>`(Optional) - an optional description to show with your event   

##### Example

`!trial vHRC 24/04/2021 17:00 Veteran Hel Ra Citadel run for teh lolz` 

Veteran Hel Ra Citadel on the 24<sup>th</sup> April 2021 @ 17:00 with a description of "Veteran Hel Ra Citadel run for teh lolz"