# Sign-in / Sign-Out of Events

## Sign-in
!!! note 
    You can only be signed into a single role for an event. If you wish to change role, please sign out and sign in.
To sign-in to an event, select your role from the reaction list added by the Bot

![Sign-In-Out](../assets/Sign-In-Out.png)

##### Role Emoji's
* :shield: - Tank  
* :sparkling_heart: - Healer  
* :crossed_swords: - Damage Dealer  

## Sign-Out

To sign-out of an event, select :x: the reaction list added by the Bot