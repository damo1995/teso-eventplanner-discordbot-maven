package uk.co.bodgeandlegit.ContextMenus;

import com.jagrosh.jdautilities.command.MessageContextMenu;
import com.jagrosh.jdautilities.command.MessageContextMenuEvent;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.exceptions.ErrorHandler;
import net.dv8tion.jda.api.requests.ErrorResponse;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Event;
import uk.co.bodgeandlegit.Main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SummonEventMessageContextMenu extends MessageContextMenu {

    private static final Logger logger = LoggerFactory.getLogger(SummonEventMessageContextMenu.class);
    private Main main;
    private DataAccessLayer db;

    public SummonEventMessageContextMenu(Main main){
        this.name = "Summon Event Users";
        this.main = main;
        this.db = main.db;
    }
    @Override
    protected void execute(MessageContextMenuEvent triggeredEvent) {
        triggeredEvent.deferReply(true).queue();
        String id = triggeredEvent.getInteraction().getTarget().getId();
        String channelId = triggeredEvent.getInteraction().getChannel().getId();
        logger.debug("Message ID: " + id);
        logger.debug("Channel ID: " + channelId);
        if(db.isEventMessage(id)){

            Event event = main.db.getEventByMessageId(id);
            List<String> signUps = Stream.of(event.getDds(),event.getHealers(),event.getTanks()).flatMap(Collection::stream).collect(Collectors.toList());
            MessageEmbed embed =  main.utils.generateSummonMessageEmbed(triggeredEvent.getUser(), event, null);

            List<User> failedUsers = main.utils.summonUsers(signUps, embed);

            StringBuilder sb = new StringBuilder();

            if(failedUsers.size() > 0){
                sb.append("We had some issues summoning the below users master.. \n");
                for (User u: failedUsers) {
                    sb.append(u.getName() + "\n");
                }
            }else{
                sb.append("All event Sign-ups have been summoned master.");
            }

            triggeredEvent.getHook().sendMessage(sb.toString()).setEphemeral(true).queue();
        }else{
            triggeredEvent.getHook().sendMessage(MessageCreateData.fromContent("You selected a message that was not a valid event! You can now dismiss this message.")).queue();
        }
    }
}
