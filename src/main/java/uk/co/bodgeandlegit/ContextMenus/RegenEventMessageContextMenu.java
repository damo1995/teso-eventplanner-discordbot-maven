package uk.co.bodgeandlegit.ContextMenus;

import com.jagrosh.jdautilities.command.MessageContextMenu;
import com.jagrosh.jdautilities.command.MessageContextMenuEvent;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.interactions.components.ItemComponent;
import net.dv8tion.jda.api.interactions.components.selections.SelectMenu;
import net.dv8tion.jda.api.interactions.components.selections.SelectOption;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Main;

import java.util.Arrays;
import java.util.Collection;

public class RegenEventMessageContextMenu extends MessageContextMenu {

    private static final Logger logger = LoggerFactory.getLogger(RegenEventMessageContextMenu.class);
    private Main main;
    private DataAccessLayer db;

    public RegenEventMessageContextMenu(Main main){
        this.name = "Regen Event Message";
        this.main = main;
        this.db = main.db;
    }
    @Override
    protected void execute(MessageContextMenuEvent event) {
        event.deferReply(true).queue();
        String id = event.getInteraction().getTarget().getId();
        String channelId = event.getInteraction().getChannel().getId();
        logger.debug("Message ID: " + id);
        logger.debug("Channel ID: " + channelId);
        if(db.isEventMessage(id)){
            main.utils.regenerateMessage(id, channelId);
            event.getHook().sendMessage(MessageCreateData.fromContent("Message Regenerated! You can now dismiss this message.")).queue();
        }else{
            event.getHook().sendMessage(MessageCreateData.fromContent("You selected a message that was not a valid event! You can now dismiss this message.")).queue();
        }

        /*event.reply("Does Dan Eat Penis?").addActionRow(
                SelectMenu.create("Does Dan Eat Penis?")
                        .addOption("Yes","Yes",Emoji.fromUnicode("\uD83C\uDF46"))
                        .addOption("No, But yes","No, But yes",Emoji.fromUnicode("\uD83C\uDF46"))
                        .build()
        ).queue();*/
    }
}
