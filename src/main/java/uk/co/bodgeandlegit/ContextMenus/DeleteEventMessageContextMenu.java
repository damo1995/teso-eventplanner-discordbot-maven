package uk.co.bodgeandlegit.ContextMenus;

import com.jagrosh.jdautilities.command.MessageContextMenu;
import com.jagrosh.jdautilities.command.MessageContextMenuEvent;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Main;

public class DeleteEventMessageContextMenu extends MessageContextMenu {

    private static final Logger logger = LoggerFactory.getLogger(DeleteEventMessageContextMenu.class);
    private Main main;
    private DataAccessLayer db;

    public DeleteEventMessageContextMenu(Main main){
        this.name = "Delete Event";
        this.main = main;
        this.db = main.db;
    }
    @Override
    protected void execute(MessageContextMenuEvent event) {
        event.deferReply(true).queue();
        String id = event.getInteraction().getTarget().getId();
        String channelId = event.getInteraction().getChannel().getId();
        logger.debug("Message ID: " + id);
        logger.debug("Channel ID: " + channelId);
        logger.debug("EventName: " + event.getName());
        if(db.isEventMessage(id)){
            event.getHook().sendMessage("Are you sure you wish to delete message " + id + " ?")
                    .addActionRow(
                            Button.danger("bbde-conf-" + id, "Yes"),
                            Button.success("bbde-rej-" + id,"No")).queue();

        }else{
            event.getHook().sendMessage(MessageCreateData.fromContent("You selected a message that was not a valid BodgeBot event!")).queue();
        }

        /*event.reply("Does Dan Eat Penis?").addActionRow(
                SelectMenu.create("Does Dan Eat Penis?")
                        .addOption("Yes","Yes",Emoji.fromUnicode("\uD83C\uDF46"))
                        .addOption("No, But yes","No, But yes",Emoji.fromUnicode("\uD83C\uDF46"))
                        .build()
        ).queue();*/
    }
}
