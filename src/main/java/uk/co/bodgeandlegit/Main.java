package uk.co.bodgeandlegit;

import com.jagrosh.jdautilities.command.CommandClient;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.requests.GatewayIntent;
import uk.co.bodgeandlegit.CommandHandlers.*;
import uk.co.bodgeandlegit.CommandHandlers.Admin.BotAdmin.BotAdminCommand;
import uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin.EventAdminCommand;
import uk.co.bodgeandlegit.CommandHandlers.Admin.InstanceAdmin.InstanceAdminCommand;
import uk.co.bodgeandlegit.CommandHandlers.AnimalCommands.*;
import uk.co.bodgeandlegit.ContextMenus.DeleteEventMessageContextMenu;
import uk.co.bodgeandlegit.ContextMenus.RegenEventMessageContextMenu;
import uk.co.bodgeandlegit.ContextMenus.SummonEventMessageContextMenu;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.EventHandlers.ButtonListener;
import uk.co.bodgeandlegit.EventHandlers.ModalInteractionListener;
import uk.co.bodgeandlegit.EventHandlers.ReactListener;
import uk.co.bodgeandlegit.Music.GuildMusicManager;
import uk.co.bodgeandlegit.Utils.DadJokeUtils;
import uk.co.bodgeandlegit.Utils.FurryUtils;
import uk.co.bodgeandlegit.Utils.MessageUtils;
import java.util.HashMap;
import java.util.Map;

public class Main {

    //JDA and Other Class Requirements.
    public DataAccessLayer db;
    public MessageUtils utils;
    public CommandClient cc;
    public JDA jda;
    public Config config = new Config();
    public EventWaiter eventWaiter = new EventWaiter();

    public final AudioPlayerManager playerManager;
    public final Map<Long, GuildMusicManager> musicManagers;
    //private RabbitMQSubscriber rabbitMQSubscriber;

    //Utils
    public DadJokeUtils bodgefatherYivo = new DadJokeUtils(config);
    public FurryUtils skyriderDragoon = new FurryUtils(config);

    //Serious commands for Event Management
    private final NewTrialCommand newTrialCommand = new NewTrialCommand(this);
    private final NewDungeonCommand newDungeonCommand = new NewDungeonCommand(this);
    private final NewPledgeCommand newPledgeCommand = new NewPledgeCommand(this);
    private final NewCustomEventCommand newCustomEventCommand = new NewCustomEventCommand(this);
    private final InstanceListNewCommand instanceListNewCommand = new InstanceListNewCommand(this);
    private final InstanceAdminCommand instanceAdminCommand = new InstanceAdminCommand(this);

    private final EventAdminCommand eventAdminCommand = new EventAdminCommand(this);
    private final BotAdminCommand botAdminCommand = new BotAdminCommand(this);

    //Fun Commands
    private final iCanHazBodgeJokeCommand iCanHazBodgeJokeCommand = new iCanHazBodgeJokeCommand(this);
    private final iCanHazCatCommand iCanHazCatCommand = new iCanHazCatCommand(this);
    private final iCanHazDoggoCommand iCanHazDoggoCommand = new iCanHazDoggoCommand(this);
    private final iCanHazDuckCommand iCanHazDuckCommand = new iCanHazDuckCommand(this);
    private final iCanHazFoxCommand iCanHazFoxCommand = new iCanHazFoxCommand(this);
    private final iCanHazPigCommand iCanHazPigCommand = new iCanHazPigCommand(this);
    private final iCanHazRabbitCommand iCanHazRabbitCommand = new iCanHazRabbitCommand(this);
    private final iCanHazRandomImageCommand iCanHazRandomImageCommand = new iCanHazRandomImageCommand(this);
    private final iCanHazGoatCommand iCanHazGoatCommand = new iCanHazGoatCommand(this);
    private final FusRoDahCommand fusRoDahCommand = new FusRoDahCommand(this);

    private Main(){
        this.musicManagers = new HashMap<Long, GuildMusicManager>();
        this.playerManager = new DefaultAudioPlayerManager();
        AudioSourceManagers.registerRemoteSources(playerManager);
        AudioSourceManagers.registerLocalSource(playerManager);
    }

    public static void main(String[] args) {

        Main mainClass = new Main();
        try {
            mainClass.startBot();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void startBot() {

        //Assign Database and Util Objects
        db = new DataAccessLayer(this, config.getDbHost(),config.getDbName(),config.getDbUsername(),config.getDbPassword());
        utils = new MessageUtils(this);

        //Define JDA object and set Intents
        JDABuilder jdaBuilder = JDABuilder.createDefault(config.getBotToken());
        jdaBuilder.enableIntents(GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_MESSAGES, GatewayIntent.MESSAGE_CONTENT, GatewayIntent.GUILD_EMOJIS_AND_STICKERS);

        //Create CommandClientBuilder class from JDA-Chewtills
        CommandClientBuilder client = new CommandClientBuilder();

        //Set Basic Bot Information
        client.setEmojis("\uD83D\uDE03", "\uD83D\uDE2E", "\uD83D\uDE26");
        client.setPrefix(config.getCmdPrefix());
        client.setOwnerId(config.getOwnerId());
        client.setActivity(Activity.listening("your Event Requests!"));
        client.setStatus(OnlineStatus.ONLINE);
        client.useHelpBuilder(false);

        //register Slash Commands
        client.addSlashCommand(newTrialCommand);
        client.addSlashCommand(newDungeonCommand);
        client.addSlashCommand(newPledgeCommand);
        client.addSlashCommand(newCustomEventCommand);
        client.addSlashCommand(instanceListNewCommand);
        client.addSlashCommand(instanceAdminCommand);
        client.addSlashCommand(botAdminCommand);
        client.addSlashCommand(eventAdminCommand);
        client.addSlashCommand(fusRoDahCommand);
        client.addSlashCommand(iCanHazBodgeJokeCommand);
        client.addSlashCommand(iCanHazDoggoCommand);
        client.addSlashCommand(iCanHazDuckCommand);
        client.addSlashCommand(iCanHazFoxCommand);
        client.addSlashCommand(iCanHazRabbitCommand);
        client.addSlashCommand(iCanHazGoatCommand);

        //register Legacy Commands
        client.addCommand(newTrialCommand);
        client.addCommand(newDungeonCommand);
        client.addCommand(newPledgeCommand);
        client.addCommand(newCustomEventCommand);
        client.addCommand(instanceListNewCommand);
        client.addCommand(instanceAdminCommand);
        client.addCommand(botAdminCommand);
        client.addCommand(eventAdminCommand);
        client.addCommand(fusRoDahCommand);
        client.addCommand(iCanHazBodgeJokeCommand);
        client.addCommand(iCanHazDoggoCommand);
        client.addCommand(iCanHazDuckCommand);
        client.addCommand(iCanHazFoxCommand);
        client.addCommand(iCanHazRabbitCommand);
        client.addCommand(iCanHazGoatCommand);
        //client.addCommand(new HelpCommand(this));

        //Additional Checks Required for API's that require keys.
        if(config.getTheCatAPIKey() != null && config.getAnimalDiscoveryChannelId() != null){
            client.addCommand(iCanHazCatCommand);
            client.addSlashCommand(iCanHazCatCommand);
        }

        if(config.getUnsplashAPIKey() != null && config.getAnimalDiscoveryChannelId() != null){
            client.addCommand(iCanHazRandomImageCommand);
            client.addSlashCommand(iCanHazRandomImageCommand);
            client.addCommand(iCanHazPigCommand);
            client.addSlashCommand(iCanHazPigCommand);
        }

        //Register Context Menus
        client.addContextMenu(new RegenEventMessageContextMenu(this));
        client.addContextMenu(new SummonEventMessageContextMenu(this));
        //client.addContextMenu(new DeleteEventMessageContextMenu(this));
        cc = client.build();

        //Register the JDA-Chewtills Objects
        jdaBuilder.addEventListeners(cc);
        jdaBuilder.addEventListeners(eventWaiter);

        //Register Event Listeners for Reactions, Buttons and InstanceEdit Modals
        jdaBuilder.addEventListeners(new ReactListener(this));
        jdaBuilder.addEventListeners(new ButtonListener()); //Not actively used at present.
        jdaBuilder.addEventListeners(new ModalInteractionListener(this));

        //Build the Bot & Connect
        this.jda = jdaBuilder.build();

        //this.rabbitMQSubscriber = new RabbitMQSubscriber(this);
    }

    public synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
        long guildId = Long.parseLong(guild.getId());
        GuildMusicManager musicManager = musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(playerManager, guild.getJDA(), this,guildId);
            musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }

}
