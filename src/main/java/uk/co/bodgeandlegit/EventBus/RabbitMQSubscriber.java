package uk.co.bodgeandlegit.EventBus;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import uk.co.bodgeandlegit.Main;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQSubscriber {
    ConnectionFactory factory;
    Connection connection;
    Channel channel;
    Main main;
    public RabbitMQSubscriber(Main main){
        this.main = main;
        this.factory = new ConnectionFactory();
        try {
            this.setupSubscribers();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

    }

    public void setupSubscribers() throws IOException, TimeoutException {

        factory.setHost("127.0.0.1");
        factory.setUsername("guest");
        factory.setPassword("guest");

        connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare("dailyBodgeJoke", false, false, false, null);
        channel.queueDeclare("dailyCuteAnimal", false, false, false, null);

        DeliverCallback deliveryDailyBodgeJoke = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            main.jda.getTextChannelById(message).sendMessage(main.bodgefatherYivo.getRandomDadJoke()).queue();

            System.out.println(" [x] Received '" + message + "'");
        };
        DeliverCallback deliveryDailyCuteAnimal = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");

            System.out.println(" [x] Received '" + message + "'");
        };
        channel.basicConsume("dailyBodgeJoke", true, deliveryDailyBodgeJoke, consumerTag -> { });
        channel.basicConsume("dailyCuteAnimal", true, deliveryDailyCuteAnimal, consumerTag -> { });
    }
}
