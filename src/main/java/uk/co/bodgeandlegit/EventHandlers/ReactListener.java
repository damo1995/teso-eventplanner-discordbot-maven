package uk.co.bodgeandlegit.EventHandlers;

import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Main;
import uk.co.bodgeandlegit.Utils.RoleType;


public class ReactListener extends ListenerAdapter {

    private final DataAccessLayer db;
    private final Main main;

    private static final Logger logger = LoggerFactory.getLogger(ReactListener.class);

    public ReactListener(Main instance){
        this.main = instance;
        this.db = main.db;
    }

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {

        if (event.getUser().isBot()) {
            return;
        }

            if (db.isEventMessage(event.getMessageId())) {

                if (event.getReaction().getEmoji().getType().equals(Emoji.Type.UNICODE)) {


                    if (event.getReaction().getEmoji().asUnicode().getAsCodepoints().equals("U+1f6e1")) {

                        //Tank
                        if (!db.isMemberSignedUp(event.getMessageId(), event.getMember().getId())) {
                            db.checkInUser(event.getMessageId(), event.getMember().getId(), RoleType.TANK, event.getChannel().getId());
                            //db.insertAuditLogEntry(event.getUser().getName(), event.getMember().getId(), "signup","{\"messageId\": \"" + event.getMessageId() + "\", \"role\": \"tank\"}");
                        }


                    } //Healer
                    else if (event.getReaction().getEmoji().asUnicode().getAsCodepoints().equals("U+1f496")) {
                        if (!db.isMemberSignedUp(event.getMessageId(), event.getMember().getId())) {
                            db.checkInUser(event.getMessageId(), event.getMember().getId(), RoleType.HEALER, event.getChannel().getId());
                            //db.insertAuditLogEntry(event.getUser().getName(), event.getMember().getId(), "signup","{\"messageId\": \"" + event.getMessageId() + "\", \"role\": \"healer\"}");
                        }
                    } //DD
                    else if (event.getReaction().getEmoji().asUnicode().getAsCodepoints().equals("U+2694U+fe0f")) {
                        if (!db.isMemberSignedUp(event.getMessageId(), event.getMember().getId())) {
                            db.checkInUser(event.getMessageId(), event.getMember().getId(), RoleType.DD, event.getChannel().getId());
                            //db.insertAuditLogEntry(event.getUser().getName(), event.getMember().getId(), "signup","{\"messageId\": \"" + event.getMessageId() + "\", \"role\": \"dd\"}");
                        }
                    } //RequiresMech
                    else if (event.getReaction().getEmoji().asUnicode().getAsCodepoints().equals("U+2753")) {
                        if (db.isMemberSignedUp(event.getMessageId(), event.getMember().getId())) {
                            db.flagRequiresMech(event.getMessageId(), event.getMember().getId(), event.getChannel().getId());
                            //db.insertAuditLogEntry(event.getUser().getName(), event.getMember().getId(), "signup","{\"messageId\": \"" + event.getMessageId() + "\", \"role\": \"dd\"}");
                        }
                    } //SIGNOUT
                    else if (event.getReaction().getEmoji().asUnicode().getAsCodepoints().equals("U+274c")) {
                        db.checkOutUser(event.getMessageId(), event.getMember().getId(), event.getChannel().getId());
                        // db.insertAuditLogEntry(event.getUser().getName(), event.getMember().getId(), "signout","{\"messageId\": \"" + event.getMessageId() + "\"}");
                    }

                    event.getReaction().removeReaction(event.getUser()).queue();

                }
            }
        }
}
