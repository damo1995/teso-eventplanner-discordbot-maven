package uk.co.bodgeandlegit.EventHandlers;

import com.mongodb.lang.NonNull;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.ModalInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Instance;
import uk.co.bodgeandlegit.Main;
import uk.co.bodgeandlegit.Utils.InstanceType;


public class ModalInteractionListener extends ListenerAdapter {

    Logger logger = LoggerFactory.getLogger(ModalInteractionEvent.class);
    Main main;

    public ModalInteractionListener(Main instance){
        this.main = instance;
    }

    @Override
    public void onModalInteraction(@NonNull ModalInteractionEvent event){
        if(event.getModalId().startsWith("bbei-")){

            logger.debug("EditInstance Modal Triggered");
            event.deferReply(true).queue();
            InstanceType type = InstanceType.valueOf(event.getModalId().split("-")[1]);
            String orgShortCode = event.getModalId().split("-")[2];

            Instance existingInstance = main.db.getInstanceByShortCode(orgShortCode, type);
            if(existingInstance != null) {

                String newName = event.getInteraction().getValue("name").getAsString();
                String newShortCode = event.getInteraction().getValue("shortCode").getAsString();
                int ddCount;
                int healerCount;
                int tankCount;
                if (isNumeric(event.getInteraction().getValue("ddcount").getAsString())) {ddCount = Integer.parseInt(event.getInteraction().getValue("ddcount").getAsString());} else {event.getHook().sendMessage("Number of Damage Dealers was not provided as a number!").queue();return;}
                if (isNumeric(event.getInteraction().getValue("healerCount").getAsString())) {healerCount = Integer.parseInt(event.getInteraction().getValue("healerCount").getAsString());} else {event.getHook().sendMessage("Number of Healers was not provided as a number!").queue();return;}
                if (isNumeric(event.getInteraction().getValue("tankCount").getAsString())) {tankCount = Integer.parseInt(event.getInteraction().getValue("tankCount").getAsString());} else {event.getHook().sendMessage("Number of Tanks was not provided as a number!").queue();return;}

                if(existingInstance.getName() != newName){ existingInstance.setName(newName);}
                if(existingInstance.getShortname() != orgShortCode){ existingInstance.setShortname(newShortCode);}
                if(existingInstance.getRoleDDCount() != ddCount){ existingInstance.setRoleDDCount(ddCount);}
                if(existingInstance.getRoleHealerCount() != healerCount){ existingInstance.setRoleHealerCount(healerCount);}
                if(existingInstance.getRoleTankCount() != tankCount){ existingInstance.setRoleTankCount(tankCount);}
                if(main.db.updateInstance(existingInstance)){
                    event.getHook().sendMessage("Updated Instance!").queue();
                }else{
                    event.getHook().sendMessage("Error updating Instance!").queue();
                }
            }
        }
        if(event.getModalId().startsWith("bbni-")){
            logger.debug("CreateInstance Modal Triggered");
            event.deferReply(true).queue();

            String name = event.getInteraction().getValue("name").getAsString();
            InstanceType type = InstanceType.valueOf(event.getModalId().split("-")[1]);
            String shortCode = event.getInteraction().getValue("shortCode").getAsString();
            int ddCount;
            int healerCount;
            int tankCount;

            if(isNumeric(event.getInteraction().getValue("ddcount").getAsString())){ddCount = Integer.parseInt(event.getInteraction().getValue("ddcount").getAsString());}else{event.getHook().sendMessage("Number of Damage Dealers was not provided as a number!").queue();return;}
            if(isNumeric(event.getInteraction().getValue("healerCount").getAsString())){healerCount = Integer.parseInt(event.getInteraction().getValue("healerCount").getAsString());}else{event.getHook().sendMessage("Number of Healers was not provided as a number!").queue();return;}
            if(isNumeric(event.getInteraction().getValue("tankCount").getAsString())){tankCount = Integer.parseInt(event.getInteraction().getValue("tankCount").getAsString());}else{event.getHook().sendMessage("Number of Tanks was not provided as a number!").queue(); return;}

            if(main.db.getInstanceByShortCode(shortCode, type) == null) {
                if (main.db.createNewInstance(name, shortCode, type, ddCount, healerCount, tankCount, event.getInteraction().getUser().getId())) {
                    EmbedBuilder eb = new EmbedBuilder();
                    eb.setDescription("Created new " + type.name().toLowerCase() + " instance!");
                    eb.addField("Instance Name:", name, false);
                    eb.addField("Instance ShortCode:", shortCode, false);
                    eb.addField("Instance Type:", type.name().toLowerCase(), false);

                    StringBuilder sb = new StringBuilder();
                    sb.append("\u2694\uFE0F: ").append(ddCount).append("\n");
                    sb.append("\uD83D\uDC96: ").append(healerCount).append("\n");
                    sb.append("\uD83D\uDEE1: ").append(tankCount).append("\n");

                    eb.addField("Roles:", sb.toString(), false);

                    event.getHook().sendMessage(MessageCreateData.fromEmbeds(eb.build())).queue();
                } else {
                    event.getHook().sendMessage("We had an issue whilst storing the new instance. Please contact " + event.getJDA().getUserById(main.config.getOwnerId()).getAsMention()).queue();
                }
            }else{
                event.getHook().sendMessage("An instance with the type " + type.name().toLowerCase() + " and shortcode " + shortCode + " already exists, Please use the edit command!").queue();
            }
        }
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

}
