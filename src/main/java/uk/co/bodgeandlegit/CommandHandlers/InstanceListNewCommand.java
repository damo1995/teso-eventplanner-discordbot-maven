package uk.co.bodgeandlegit.CommandHandlers;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import com.jagrosh.jdautilities.commons.utils.TableBuilder;
import com.jagrosh.jdautilities.menu.Paginator;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Instance;
import uk.co.bodgeandlegit.Main;
import uk.co.bodgeandlegit.Utils.InstanceType;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class InstanceListNewCommand extends SlashCommand {

    private final Main main;
    private final DataAccessLayer db;

    private final Logger logger = LoggerFactory.getLogger(InstanceListNewCommand.class);
    String rowDelimiter = "\u2500";
    String columnDelimiter= "\u2502";
    String crossDelimiter = "\u253C";
    String leftIntersection = "\u251C";
    String rightIntersection = "\u2524";
    String upperIntersection=  "\u252C";
    String lowerIntersection=  "\u2534";
    String upLeftCorner= "\u250C";
    String upRightCorner= "\u2510";
    String lowLeftCorner= "\u2514";
    String lowRightCorner= "\u2518";
    String headerDelimiter= "\u2550";
    String headerCrossDelimiter="\u256A";
    String headerLeftIntersection= "\u255E";
    String headerRightIntersection="\u2561";
    String firstColumnDelimiter="\u2551";
    String firstColumnCrossDelimiter="\u256B";
    String firstColumnUpperIntersection="\u2565";
    String firstColumnLowerIntersection="\u2568";
    String headerColumnCrossDelimiter="\u256C";
    String horizontalOutline="\u2500";
    String verticalOutline="\u2502";

    private TableBuilder.Borders bordersToUse = TableBuilder.Borders.newHeaderRowNamesFrameBorders(rowDelimiter, columnDelimiter, crossDelimiter, leftIntersection, rightIntersection, upperIntersection, lowerIntersection, upLeftCorner, upRightCorner, lowLeftCorner, lowRightCorner, headerDelimiter, headerCrossDelimiter, headerLeftIntersection, headerRightIntersection, firstColumnDelimiter, firstColumnCrossDelimiter, firstColumnUpperIntersection, firstColumnLowerIntersection, headerColumnCrossDelimiter, horizontalOutline, verticalOutline);

    public InstanceListNewCommand(Main instance){
        this.main = instance;
        this.db = main.db;

        this.name = "instances";
        this.arguments = "<dungeon/trial/custom>";
        this.help = "List known instances";
        this.category = new Category("Instance List");
        this.guildOnly = true;
        OptionData od = new OptionData(OptionType.STRING, "type","The instance Type").setRequired(true);

        for(InstanceType type : InstanceType.values()){
            od.addChoice(type.name(), type.name());
        }

        this.options = Arrays.asList(od);
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {
        commandEvent.reply("You should see a table below, Please dismiss this message :)").setEphemeral(true).queue();


            InstanceType instanceType = InstanceType.valueOf(commandEvent.optString("type").toUpperCase());
            Paginator.Builder pb = main.utils.generateKnownInstancesPaged(instanceType);
            pb.build().display(commandEvent.getChannel());

    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        String cmdPrefix = main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());

    }

    private void sendErrorMessage(CommandEvent commandEvent) {

        commandEvent.getMessage().delete().queue();
        StringBuilder sb = new StringBuilder();

        sb.append("Invalid Command Usage\n");
        sb.append("`!instance-list <dungeon/trial/custom>`\n");
        sb.append("Example: `!instance-list trial`");

        commandEvent.getChannel().sendMessage(sb.toString()).queue(message -> message.delete().queueAfter(20, TimeUnit.SECONDS));
    }

    private String[][] convertInstancesForList(List<Instance> instances){
        String[][] instanceArray = new String[instances.size()][2];

        for (int i = 0; i < instances.size(); i++) {
            instanceArray[i][0] = instances.get(i).getShortname();
            instanceArray[i][1] = instances.get(i).getName();
        }
        return instanceArray;
    }

}
