package uk.co.bodgeandlegit.CommandHandlers;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import com.jagrosh.jdautilities.menu.Paginator;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Main;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Instance;
import uk.co.bodgeandlegit.Utils.InstanceType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;


public class NewDungeonCommand extends SlashCommand {

    private Main main;
    private Logger logger = LoggerFactory.getLogger(NewDungeonCommand.class);


    public NewDungeonCommand(Main instance){
        this.main = instance;
        this.name = "dungeon";
        this.arguments = "<shortcode> <date> <time> <description>";
        this.help = "Create new Dungeon Event";
        this.category = new Category("Event Creation");
        this.guildOnly = true;
        this.options = Arrays.asList(
                new OptionData(OptionType.STRING, "shortcode","Dungeon Instance ShortCode").setRequired(true),
                new OptionData(OptionType.STRING, "date", "Event Date dd/mm/yyyy format").setRequired(true),
                new OptionData(OptionType.STRING, "time", "Event time hh:mm format").setRequired(true),
                new OptionData(OptionType.STRING, "description", "Optional description about the event.").setRequired(false)
        );
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {

        commandEvent.deferReply().queue();
        logger.debug("Channel ID: " + commandEvent.getChannel().getId());

        Instance esoInstanceModel = main.db.getInstanceByShortCode(commandEvent.optString("shortcode"), InstanceType.DUNGEON);

        if(esoInstanceModel != null){

            String eventDate = commandEvent.optString("date");
            String eventTime = commandEvent.optString("time");
            String eventDateTimeStr;
            LocalDateTime dateTime = null;

            DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

            if (eventDate.equalsIgnoreCase("today")) {
                eventDateTimeStr = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + eventTime;
            } else {
                eventDateTimeStr = eventDate + " " + eventTime;
            }

            try {
                dateTime = LocalDateTime.parse(eventDateTimeStr, dtFormatter);
            } catch (Exception e) {
                logger.error("Error whilst trying to convert time/date: " + eventDateTimeStr);
                logger.error(e.toString());
                commandEvent.getHook().sendMessage("Invalid date or Time Specified, Please ensure you use dd/mm/yyyy and hh:mm");
            }

            String description = commandEvent.optString("description");


            EmbedBuilder embedBuilder = main.utils.generateBlankMessage(esoInstanceModel, dateTime, description, commandEvent.getMember().getAsMention());

            String finalDescription = description;
            MessageCreateData msg = new MessageCreateBuilder().addEmbeds(embedBuilder.build()).build();

            LocalDateTime finalDateTime = dateTime;

            commandEvent.getHook().sendMessage(msg).queue(message -> {
                //Shield
                message.addReaction(Emoji.fromUnicode("\uD83D\uDEE1")).queue();
                //Heart
                message.addReaction(Emoji.fromUnicode("\uD83D\uDC96")).queue();
                //Swords
                message.addReaction(Emoji.fromUnicode("\u2694\uFE0F")).queue();
                //Mechs
                message.addReaction(Emoji.fromUnicode("\u2753")).queue();
                //Cross
                message.addReaction(Emoji.fromUnicode("\u274C")).queue();

                main.db.createNewEvent(message.getId(), commandEvent.getChannel().getId(), esoInstanceModel, commandEvent.getMember().getId(), finalDescription, finalDateTime);
            });

        }else {
            commandEvent.getHook().sendMessage("Unknown Dungeon Shortcode: " + commandEvent.optString("shortcode")).queue();
        }

    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        String cmdPrefix = main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());
    }

}
