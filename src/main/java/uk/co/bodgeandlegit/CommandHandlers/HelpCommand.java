package uk.co.bodgeandlegit.CommandHandlers;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import uk.co.bodgeandlegit.Main;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class HelpCommand extends Command {

    Main main;

    public HelpCommand(Main main){
        this.main = main;
        this.name = main.config.getHelpCmd();
        this.hidden = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        StringBuilder builder = new StringBuilder("**"+event.getSelfUser().getName()+"** commands:\n");
        Category category = null;

        for(Command command : main.cc.getCommands())
        {
            if(!command.isHidden() && (!command.isOwnerCommand() || event.isOwner()) && (command.getRequiredRole() == null || event.getMember().getRoles().stream().anyMatch(r -> r.getName().equalsIgnoreCase(main.config.getAdminRole()))))
            {
                if(!Objects.equals(category, command.getCategory()))
                {
                    category = command.getCategory();
                    builder.append("\n\n  __").append(category==null ? "No Category" : category.getName()).append("__:\n");
                }
                builder.append("\n`").append(main.cc.getPrefix()).append(command.getName())
                        .append(command.getArguments()==null ? "`" : " "+command.getArguments()+"`")
                        .append(" - ").append(command.getHelp());
                if(command.getChildren().length > 0){
                    builder.append("\n\nSub-Commands:\n");
                    for(Command childCMD : command.getChildren()){
                        builder.append("\n`").append(childCMD.getName())
                                .append(childCMD.getArguments()==null ? "`" : " "+childCMD.getArguments()+"`")
                                .append(" - ").append(childCMD.getHelp());
                    }
                }
            }
        }
        User owner = event.getJDA().getUserById(main.cc.getOwnerId());
        if(owner!=null)
        {
            builder.append("\n\nFor additional help, contact **").append(owner.getName()).append("**#").append(owner.getDiscriminator());
        }
        event.replyInDm(builder.toString(), unused ->
        {
            if(event.isFromType(ChannelType.TEXT))
                event.getMessage().delete().queue();
        }, t -> event.getChannel().sendMessage(main.cc.getWarning() +  "  Help cannot be sent because you are blocking Direct Messages.").queue(s -> s.delete().queueAfter(30, TimeUnit.SECONDS)));

    }
}
