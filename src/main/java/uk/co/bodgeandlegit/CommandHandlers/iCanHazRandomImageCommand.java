package uk.co.bodgeandlegit.CommandHandlers;

import com.jagrosh.jdautilities.command.*;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pw.chew.jdachewtils.command.OptionHelper;
import uk.co.bodgeandlegit.Main;

import java.util.Collections;
import java.util.NoSuchElementException;


public class iCanHazRandomImageCommand extends SlashCommand {

    private final Main main;

    private final Logger logger = LoggerFactory.getLogger(iCanHazRandomImageCommand.class);

    public iCanHazRandomImageCommand(Main instance){

        this.main = instance;
        this.cooldownScope = CooldownScope.USER_GUILD;
        this.cooldown = 60;
        this.name = "icanhazrandomimage";
        this.help = "Returns a random image based on the search criteria";
        this.category = new Category("Misc");
        this.arguments = "<search string>(optional)";
        this.options = Collections.singletonList(new OptionData(OptionType.STRING, "search", "Search String").setRequired(false));
        this.guildOnly = true;
    }

    @Override
    protected void execute(SlashCommandEvent event) {
        event.deferReply().queue();
        String searchTerm = event.optString("search");

        try {
            MessageEmbed messageEmbed = main.skyriderDragoon.getRandomImage(searchTerm);

            if (messageEmbed != null) {
                event.getHook().sendMessage(MessageCreateData.fromEmbeds(messageEmbed)).queue();
            } else {
                event.getHook().sendMessage("Unable to connect to the random image api! :(");
            }
        }catch (NoSuchElementException e){
            event.getHook().sendMessage("Unable to find image with search term: " + searchTerm);
        }

    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        String cmdPrefix = main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());

    }
}

