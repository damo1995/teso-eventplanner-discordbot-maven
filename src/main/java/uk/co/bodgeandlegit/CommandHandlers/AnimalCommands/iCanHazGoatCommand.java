package uk.co.bodgeandlegit.CommandHandlers.AnimalCommands;

import com.jagrosh.jdautilities.command.*;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.managers.AudioManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Main;
import uk.co.bodgeandlegit.Music.GuildMusicManager;


public class iCanHazGoatCommand extends SlashCommand {

    private final Main main;

    private final Logger logger = LoggerFactory.getLogger(iCanHazGoatCommand.class);

    public iCanHazGoatCommand(Main instance){

        this.main = instance;
        this.cooldownScope = CooldownScope.GUILD;
        this.cooldown = 60;
        this.name = "icanhazgoat";
        this.aliases = new String[]{"battlecry"};
        this.help = "Begin the BattleCry!";
        this.category = new Category("Misc");
        this.guildOnly = true;
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {
        Member author = commandEvent.getMember();

        if(author.getVoiceState().inAudioChannel()){
            commandEvent.deferReply(true).queue();

            GuildMusicManager musicManager = main.getGuildAudioPlayer(commandEvent.getGuild());

            AudioManager am = commandEvent.getGuild().getAudioManager();
            AudioChannel vc = author.getVoiceState().getChannel();
            if(!am.isConnected()){
                am.openAudioConnection(vc);
            }


            main.playerManager.loadItem("https://www.youtube.com/watch?v=X78GyIfrSV4", new AudioLoadResultHandler() {
                @Override
                public void trackLoaded(AudioTrack track) {
                    musicManager.scheduler.queue(track);
                    //play(channel, musicManager, track);
                }

                @Override
                public void playlistLoaded(AudioPlaylist playlist) {
                    for (AudioTrack track : playlist.getTracks()) {
                        musicManager.scheduler.queue(track);
                    }
                }

                @Override
                public void noMatches() {
                    System.out.println("No Matches hit!");
                }

                @Override
                public void loadFailed(FriendlyException exception) {
                    System.out.println("Load Failed!");
                    exception.printStackTrace();
                }
            });
            commandEvent.getHook().sendMessage("I hope you enjoyed your goating!").queue();

        }else{
            commandEvent.reply("You are not in a voice channel!").setEphemeral(true).queue();
        }

    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        String cmdPrefix = main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());

    }
}

