package uk.co.bodgeandlegit.CommandHandlers.AnimalCommands;

import com.jagrosh.jdautilities.command.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Main;


public class iCanHazBodgeJokeCommand extends SlashCommand {

    private final Main main;

    private final Logger logger = LoggerFactory.getLogger(iCanHazBodgeJokeCommand.class);

    public iCanHazBodgeJokeCommand(Main instance){

        this.main = instance;
        this.guildOnly = true;
        this.cooldownScope = CooldownScope.USER_GUILD;
        this.cooldown = 60;
        this.name = "icanhazbodgejoke";
        this.help = "Returns a random Dad Joke";
        this.category = new Category("Misc");
    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        String cmdPrefix = main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());
    }

    @Override
    protected void execute(SlashCommandEvent event){
        event.deferReply().queue();
        event.getHook().sendMessage(main.bodgefatherYivo.getRandomDadJoke()).queue();

    }
}

