package uk.co.bodgeandlegit.CommandHandlers.AnimalCommands;

import com.jagrosh.jdautilities.command.*;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Main;


public class iCanHazDuckCommand extends SlashCommand {

    private final Main main;

    private final Logger logger = LoggerFactory.getLogger(iCanHazDuckCommand.class);

    public iCanHazDuckCommand(Main instance){

        this.main = instance;
        this.cooldownScope = CooldownScope.USER_GUILD;
        this.cooldown = 60;
        this.name = "icanhazduck";
        this.help = "Returns a random image of a Duck";
        this.category = new Category("Misc");
        this.guildOnly = true;
    }

    @Override
    protected void execute(SlashCommandEvent event) {
        if(event.getChannel().getId().equals(main.config.getAnimalDiscoveryChannelId())) {
            event.deferReply().queue();
            MessageEmbed messageEmbed = main.skyriderDragoon.getRandomDuck();
            if(messageEmbed != null){
                event.getHook().sendMessage(MessageCreateData.fromEmbeds(messageEmbed)).queue();
            }else {
                event.getHook().sendMessage("Unable to connect to the ducky api! :(").queue();
            }
        }else{
            event.reply("You can only use that command in " + event.getJDA().getGuildChannelById(main.config.getAnimalDiscoveryChannelId()).getAsMention()).setEphemeral(true).queue();
        }

    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        String cmdPrefix = main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());

    }
}

