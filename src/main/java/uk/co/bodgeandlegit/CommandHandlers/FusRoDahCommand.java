package uk.co.bodgeandlegit.CommandHandlers;

import com.jagrosh.jdautilities.command.*;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.managers.AudioManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Main;
import uk.co.bodgeandlegit.Music.GuildMusicManager;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class FusRoDahCommand extends SlashCommand {

    private final Main main;

    private final Logger logger = LoggerFactory.getLogger(FusRoDahCommand.class);

    public FusRoDahCommand(Main instance){

        this.main = instance;
        this.cooldownScope = CooldownScope.GUILD;
        this.cooldown = 60;
        this.name = "fusrodah";
        this.help = "Banish the user to the Shadow Realm!";
        this.category = new Category("Misc");
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
        this.guildOnly = true;
        this.options = Arrays.asList(
                new OptionData(OptionType.USER,"mentioneduser", "The user to banish to the shadow realm").setRequired(true));
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {

        if(commandEvent.getMember().hasPermission(Permission.ADMINISTRATOR)){
            commandEvent.deferReply(true).queue();
            Member target = commandEvent.getOption("mentioneduser").getAsMember();

            if(target.getVoiceState().inAudioChannel()){
                GuildMusicManager musicManager = main.getGuildAudioPlayer(commandEvent.getGuild());
                AudioManager am = commandEvent.getGuild().getAudioManager();
                AudioChannel vc = target.getVoiceState().getChannel();

                if(!am.isConnected()){
                    am.openAudioConnection(vc);
                }


                main.playerManager.loadItem("https://www.youtube.com/watch?v=zzH_Wn0R-yo", new AudioLoadResultHandler() {
                    @Override
                    public void trackLoaded(AudioTrack track) {
                        musicManager.scheduler.queue(track);
                        //play(channel, musicManager, track);
                    }

                    @Override
                    public void playlistLoaded(AudioPlaylist playlist) {
                        for (AudioTrack track : playlist.getTracks()) {
                            musicManager.scheduler.queue(track);
                        }
                    }

                    @Override
                    public void noMatches() {
                        System.out.println("No Matches hit!");
                    }

                    @Override
                    public void loadFailed(FriendlyException exception) {
                        System.out.println("Load Failed!");
                        exception.printStackTrace();
                    }
                });
                vc.getGuild().kickVoiceMember(target).queueAfter(4, TimeUnit.SECONDS);
                commandEvent.getHook().sendMessage("User has been banished!").queueAfter(5,TimeUnit.SECONDS);
            }else{
                commandEvent.getHook().sendMessage("User " + target.getUser().getName() + " Is not in a voice channel!");
            }
        }

        if(commandEvent.getMember().getVoiceState().inAudioChannel()){
            commandEvent.deferReply(true).queue();
            Member target = commandEvent.getOption("mentioneduser").getAsMember();


            GuildMusicManager musicManager = main.getGuildAudioPlayer(commandEvent.getGuild());

            AudioManager am = commandEvent.getGuild().getAudioManager();
            AudioChannel vc = commandEvent.getMember().getVoiceState().getChannel();

            if(!am.isConnected()){
                am.openAudioConnection(vc);
            }


            main.playerManager.loadItem("https://www.youtube.com/watch?v=zzH_Wn0R-yo", new AudioLoadResultHandler() {
                @Override
                public void trackLoaded(AudioTrack track) {
                    musicManager.scheduler.queue(track);
                    //play(channel, musicManager, track);
                }

                @Override
                public void playlistLoaded(AudioPlaylist playlist) {
                    for (AudioTrack track : playlist.getTracks()) {
                        musicManager.scheduler.queue(track);
                    }
                }

                @Override
                public void noMatches() {
                    System.out.println("No Matches hit!");
                }

                @Override
                public void loadFailed(FriendlyException exception) {
                    System.out.println("Load Failed!");
                    exception.printStackTrace();
                }
            });
                vc.getGuild().kickVoiceMember(target).queueAfter(4, TimeUnit.SECONDS);
                commandEvent.getHook().sendMessage("User has been banished!").queueAfter(5,TimeUnit.SECONDS);
        }else{
            commandEvent.reply("You are not in a voice channel!").setEphemeral(true).queue();
        }
    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        String cmdPrefix = main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());


    }
}

