package uk.co.bodgeandlegit.CommandHandlers.Admin.InstanceAdmin;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.text.TextInput;
import net.dv8tion.jda.api.interactions.components.text.TextInputStyle;
import net.dv8tion.jda.api.interactions.modals.Modal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Instance;
import uk.co.bodgeandlegit.Utils.InstanceType;

import java.util.Arrays;

public class SubEditInstanceCommand extends SlashCommand {

    InstanceAdminCommand instanceAdminCommand;

    private final String CANCEL = "\u274C"; // ❌
    private final String CONFIRM = "\u2705"; // ☑

    private int DDCountInt = 0;
    private int HealerCountInt = 0;

    private int TankCountInt = 0;
    private final Logger logger = LoggerFactory.getLogger(SubEditInstanceCommand.class);

    public SubEditInstanceCommand(InstanceAdminCommand instance){
        this.instanceAdminCommand = instance;
        this.name = "editinstance";
        this.arguments = "<Short-Code> <trial/dungeon/custom> d:<DD Count> h:<Healer Count> t:<Tank Count>";
        this.help = "Edits an existing Instance";
        this.requiredRole = instanceAdminCommand.main.config.getAdminRole();
        this.category = new Category("Event Administration");
        OptionData od = new OptionData(OptionType.STRING, "type","The Instance Type").setRequired(true);

        for(InstanceType type : InstanceType.values()){
            if(type == InstanceType.PLEDGE){continue;}
            od.addChoice(type.name(), type.name());
        }
        this.options = Arrays.asList(od, new OptionData(OptionType.STRING,"shortcode", "The shortcode of the instance you wish to edit").setRequired(true));
    }


    @Override
    protected void execute(SlashCommandEvent event) {
        String type = event.optString("type");
        String shortCode = event.optString("shortcode");
        logger.debug("Type: " + type);
        logger.debug("shortCode: " + shortCode);

        //event.deferReply(true).queue();

        InstanceType typeObject = null;
        try{
            typeObject = InstanceType.valueOf(type);
        }catch (IllegalArgumentException e){
            event.getHook().sendMessage("I'm not sure what you did here, but you focked up!").queue();
            return;
        }
        logger.debug("Type Object: " + typeObject.toString());

        Instance instance = instanceAdminCommand.main.db.getInstanceByShortCode(shortCode,typeObject);
        logger.debug("Instance object: " + instance.getName());

        if(instance != null){
            TextInput instanceName = TextInput.create("name","Instance Name", TextInputStyle.SHORT)
                                        .setPlaceholder("Enter the new instance name")
                                        .setValue(instance.getName())
                                        .build();

            TextInput shortCodeTxt = TextInput.create("shortCode","Instance ShortCode", TextInputStyle.SHORT)
                                        .setPlaceholder("Enter the new short code")
                                        .setValue(instance.getShortname())
                                        .build();

            TextInput ddCount = TextInput.create("ddcount","Number of Damage Dealers", TextInputStyle.SHORT)
                                        .setPlaceholder("Enter the number of Damage Dealers Required")
                                        .setValue(String.valueOf(instance.getRoleDDCount()))
                                        .build();

            TextInput healerCount = TextInput.create("healerCount","Number of Healers", TextInputStyle.SHORT)
                                        .setPlaceholder("Enter the number of Healers Required")
                                        .setValue(String.valueOf(instance.getRoleHealerCount()))
                                        .build();

            TextInput tankCount = TextInput.create("tankCount","Number of Healers", TextInputStyle.SHORT)
                                        .setPlaceholder("Enter the number of Tanks Required")
                                        .setValue(String.valueOf(instance.getRoleTankCount()))
                                        .build();

            String modalID = "bbei-" + typeObject.name() + "-"+ instance.getShortname();
            Modal modal = Modal.create(modalID, "Edit Instance - " + instance.getType().toString())
                    .addActionRows(
                            ActionRow.of(instanceName),
                            ActionRow.of(shortCodeTxt),
                            ActionRow.of(ddCount),
                            ActionRow.of(healerCount),
                            ActionRow.of(tankCount)
                    ).build();

            event.replyModal(modal).queue();

        }else{
            event.reply("Invalid combination of Event Type and Short Code!").queue();
        }
    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        String cmdPrefix = instanceAdminCommand.main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.subcommandGroup + " " + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());

        /*
        if (commandEvent.getChannel().getId().equals(instanceAdminCommand.main.config.getPledgeChannelId()) || commandEvent.getChannel().getId().equals(instanceAdminCommand.main.config.getTrialChannelId()) || commandEvent.getChannel().getId().equals(instanceAdminCommand.main.config.getDungeonChannelId())) {

            String combinedArgs = commandEvent.getArgs();
            String instanceName = combinedArgs.substring(combinedArgs.indexOf("\"") + 1, combinedArgs.lastIndexOf("\""));
            String[] instanceDetails = combinedArgs.substring(combinedArgs.lastIndexOf("\"") +1).trim().split(" ");

            String ShortCode = instanceDetails[0];
            InstanceType Type;

            try {
                Type = InstanceType.valueOf(instanceDetails[1].toUpperCase());
            }catch (IllegalArgumentException e){
                commandEvent.replyError("Invalid Type!");
                return;
            }

            String DDCountStr = instanceDetails[2];

            final Instance instance = instanceAdminCommand.db.getInstanceByShortCode(ShortCode, Type);

            if(instance != null){

                    String HealerCountStr = instanceDetails[3];

                    String TankCountStr = instanceDetails[4];


                    if (!(DDCountStr.matches("(?i)(d:)\\d+")) || !(HealerCountStr.matches("(?i)(h:)\\d+")) || !(TankCountStr.matches("(?i)(t:)\\d+"))) {
                        commandEvent.replyError("Invalid usage!");
                        return;
                    }

                  //DDCountInt = Integer.valueOf(DDCountStr.split(":")[1]);
                    DDCountInt = Integer.parseInt(DDCountStr.split(":")[1]);
                    HealerCountInt = Integer.parseInt(HealerCountStr.split(":")[1]);
                    TankCountInt = Integer.parseInt(TankCountStr.split(":")[1]);


                    EmbedBuilder eb = new EmbedBuilder();
                    eb.setDescription("Please confirm the below Update: ");
                    eb.setTitle("Update Instance Request");
                    eb.addField("Instance Name:", instance.getName(), false);
                    eb.addField("Instance ShortCode:", instance.getShortname(), false);
                    eb.addField("Instance Type:", instance.getType().toString(), false);

                String sb = "\u2694\uFE0F: " + DDCountInt + "\n" +
                        "\uD83D\uDC96: " + HealerCountInt + "\n" +
                        "\uD83D\uDEE1: " + TankCountInt + "\n";

                    eb.addField("Roles:", sb, false);

                    waitForConfirmation(commandEvent, eb.build(), () -> instanceAdminCommand.db.createNewInstance(instanceName, ShortCode, instance.getType(), DDCountInt, HealerCountInt, TankCountInt, commandEvent.getAuthor().getId()));

            }else {
                commandEvent.replyError("Instance with Type does not exist!");
            }
        }
    }


    private void waitForConfirmation(CommandEvent event, MessageEmbed message, Runnable confirm){

       /* CustomButtonMenu.Builder mb =  new CustomButtonMenu.Builder()
                .setUsers(event.getAuthor())
                .setEventWaiter(eventAdminCommand.main.eventWaiter)
                .setTimeout(1, TimeUnit.MINUTES)
                .setEmbed(message)
                .setChoices(CONFIRM, CANCEL)
                .setAction(re ->{
                    if(re.getName().equals(CONFIRM)) {
                        confirm.run();
                    }
                    else if(re.getName().equals(CANCEL)) {
                    }
                })
                .setFinalAction(fa -> {
                    fa.delete().queue();
                });

        mb.build().display(event.getChannel());*/
    }
}
