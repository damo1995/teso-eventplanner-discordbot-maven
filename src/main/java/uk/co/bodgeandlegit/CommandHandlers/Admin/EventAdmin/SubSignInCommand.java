package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Utils.InstanceType;
import uk.co.bodgeandlegit.Utils.RoleType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubSignInCommand extends SlashCommand {

    EventAdminCommand eventAdminCommand;
    private final Logger logger = LoggerFactory.getLogger(SubSignInCommand.class);

    public SubSignInCommand(EventAdminCommand instance) {
        this.eventAdminCommand = instance;
        this.name = "signin";
        this.arguments = "<messageid> <tank/healer/dd> <atmention user/s> ";
        this.help = "Checks in User/s with role to an event";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.category = new Category("Event Administration");
        this.guildOnly = true;
        OptionData roles = new OptionData(OptionType.STRING, "role","Role to add users too").setRequired(true);

        for(RoleType type : RoleType.values()){
            roles.addChoice(type.name(), type.name());
        }

        this.options = Arrays.asList(
                new OptionData(OptionType.STRING,"messageid", "The message ID of the event to sign users into").setRequired(true),
                roles,
                new OptionData(OptionType.STRING,"users","Users to Add to the role").setRequired(true));
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {
        String messageEventId = commandEvent.optString("messageid");

        if(eventAdminCommand.main.db.isEventMessage(messageEventId)){
            commandEvent.deferReply(true).queue();
            List<User> mentionedUsers = commandEvent.getOption("users").getMentions().getUsers();
            List<User> addUser = new ArrayList<>();
            List<User> existingUser = new ArrayList<>();
            mentionedUsers.forEach(user -> {
                if (!eventAdminCommand.main.db.isMemberSignedUp(messageEventId, user.getId())) {
                    addUser.add(user);
                    eventAdminCommand.main.db.checkInUser(messageEventId, user.getId(), RoleType.valueOf(commandEvent.optString("role")), commandEvent.getChannel().getId());
                            /*JSONObject auditLogEntry = new JSONObject();
                            auditLogEntry.put("messageId", messageEventId);
                            auditLogEntry.put("role",role);
                            auditLogEntry.put("user", user.getName());
                            auditLogEntry.put("userId", user.getId());

                            eventAdminCommand.db.insertAuditLogEntry(commandEvent.getMember().getUser().getName(), commandEvent.getMember().getId(), "admin-signup",auditLogEntry.toJSONString());*/
                } else {
                    existingUser.add(user);
                }
            });
            EmbedBuilder eb = new EmbedBuilder().setTitle("Sign in Result");
            StringBuilder sb = new StringBuilder();

            if(addUser.size() > 0 ) {
                sb.append("\u2705 **Signed in the following users** \u2705\n");
                for (User u : addUser) {
                    sb.append(u.getAsMention() + "\n");
                }
            }
            sb.append("\n");
            if(existingUser.size() >0){
                sb.append("\u274C **The following users are already signed up** \u274C\n");
                for (User u: existingUser) {
                    sb.append(u.getAsMention() + "\n");
                }
            }
            eb.setDescription(sb.toString());
            commandEvent.getHook().sendMessage(MessageCreateData.fromEmbeds(eb.build())).queue();
        }else{
            commandEvent.reply("Message ID specified was not a BodgeBot Event!").setEphemeral(true).queue();
        }
    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        if (commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getPledgeChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getTrialChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getDungeonChannelId())) {

            String[] items = commandEvent.getArgs().split("\\s+");

            String messageEventId = items[0];
            String role = items[1];

            if (eventAdminCommand.main.db.isEventMessage(messageEventId) &&
                    (role.equalsIgnoreCase("tank") || role.equalsIgnoreCase("healer") || role.equalsIgnoreCase("dd"))
            ) {

                if (commandEvent.getMessage().getMentions().getUsers().size() > 0) {
                    List<User> mentionedUsers = commandEvent.getMessage().getMentions().getUsers();
                    mentionedUsers.forEach(user -> {
                        if (!eventAdminCommand.main.db.isMemberSignedUp(messageEventId, user.getId())) {
                            eventAdminCommand.main.db.checkInUser(messageEventId, user.getId(), RoleType.valueOf(role.toUpperCase()), commandEvent.getChannel().getId());
                            /*JSONObject auditLogEntry = new JSONObject();
                            auditLogEntry.put("messageId", messageEventId);
                            auditLogEntry.put("role",role);
                            auditLogEntry.put("user", user.getName());
                            auditLogEntry.put("userId", user.getId());

                            eventAdminCommand.db.insertAuditLogEntry(commandEvent.getMember().getUser().getName(), commandEvent.getMember().getId(), "admin-signup",auditLogEntry.toJSONString());*/
                        } else {
                            commandEvent.replyInDm("User: " + user.getName() + " is already signed up for " + messageEventId);
                        }
                    });
                } else {
                    commandEvent.replyInDm("No users were specified!");
                }

            } else {
                commandEvent.replyInDm("Invalid MessageId or Role specified!");
            }

            commandEvent.getMessage().delete().queue();

        }
    }
}

