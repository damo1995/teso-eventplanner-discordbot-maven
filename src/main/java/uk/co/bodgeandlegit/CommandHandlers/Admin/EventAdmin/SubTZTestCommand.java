package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class SubTZTestCommand extends Command {

    EventAdminCommand eventAdminCommand;

    public SubTZTestCommand(EventAdminCommand instance){
        this.eventAdminCommand = instance;
        this.name = "tztest";
        this.help = "Test TimeZoning";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.category = new Category("Event Administration");
    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        if ( commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getPledgeChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getTrialChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getDungeonChannelId())) {
            Long epoch;
            Long epoch2;
            String eventDateTimeStr;
            LocalDateTime dateTime = null;
            String eventTime = "20:00";

            DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

            eventDateTimeStr = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + eventTime;

            dateTime = LocalDateTime.parse(eventDateTimeStr, dtFormatter);
            epoch = dateTime.toEpochSecond(ZoneOffset.UTC);
            epoch2 = dateTime.atZone(ZoneId.of("Europe/London")).toEpochSecond();

            StringBuilder sb = new StringBuilder();

            sb.append("Local Date Time String: " + eventDateTimeStr + "\n");
            sb.append("Local Date Time Epoch: " + epoch + " <t:" + epoch + ">\n");
            sb.append("Epoch2: " + epoch2 + " <t:" + epoch2 + ">\n");

           commandEvent.reply(sb.toString());

            commandEvent.getMessage().delete().queue();
        }
    }
}
