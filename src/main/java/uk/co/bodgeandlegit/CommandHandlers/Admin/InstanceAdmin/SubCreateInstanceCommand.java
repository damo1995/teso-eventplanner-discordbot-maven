package uk.co.bodgeandlegit.CommandHandlers.Admin.InstanceAdmin;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.text.TextInput;
import net.dv8tion.jda.api.interactions.components.text.TextInputStyle;
import net.dv8tion.jda.api.interactions.modals.Modal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Utils.InstanceType;

import java.util.Arrays;

public class SubCreateInstanceCommand extends SlashCommand {

    InstanceAdminCommand instanceAdminCommand;

    private final String CANCEL = "\u274C"; // ❌
    private final String CONFIRM = "\u2705"; // ☑

    private int DDCountInt = 0;
    private int HealerCountInt = 0;

    private int TankCountInt = 0;
    private final Logger logger = LoggerFactory.getLogger(SubCreateInstanceCommand.class);

    public SubCreateInstanceCommand(InstanceAdminCommand instance){
        this.instanceAdminCommand = instance;

        this.name = "createinstance";
        this.arguments = "\"<Instance Name>\" <Short-Code> <trial/dungeon/custom> d:<DD Count> h:<Healer Count> t:<Tank Count>";
        this.help = "Creates a new Instance";
        this.requiredRole = instanceAdminCommand.main.config.getAdminRole();
        this.category = new Category("Event Administration");
        OptionData od = new OptionData(OptionType.STRING, "type","The Instance Type").setRequired(true);

        for(InstanceType type : InstanceType.values()){
            if(type == InstanceType.PLEDGE){continue;}
            od.addChoice(type.name(), type.name());
        }
        this.options = Arrays.asList(od);
    }


    @Override
    protected void execute(SlashCommandEvent event) {
        TextInput instanceName = TextInput.create("name","Instance Name", TextInputStyle.SHORT).setPlaceholder("Enter the new instance name").build();
        TextInput shortCode = TextInput.create("shortCode","Instance ShortCode", TextInputStyle.SHORT).setPlaceholder("Enter the new short code").build();
        TextInput ddCount = TextInput.create("ddcount","Number of Damage Dealers", TextInputStyle.SHORT).setPlaceholder("Enter the number of Damage Dealers Required").build();
        TextInput healerCount = TextInput.create("healerCount","Number of Healers", TextInputStyle.SHORT).setPlaceholder("Enter the number of Healers Required").build();
        TextInput tankCount = TextInput.create("tankCount","Number of Tanks", TextInputStyle.SHORT).setPlaceholder("Enter the number of Tanks Required").build();

        String modalId = "bbni-" + event.getOption("type").getAsString();

        Modal modal = Modal.create(modalId, "Create New Instance - " + event.getOption("type").getAsString())
                .addActionRows( ActionRow.of(instanceName),
                        ActionRow.of(shortCode),
                        ActionRow.of(ddCount),
                        ActionRow.of(healerCount),
                        ActionRow.of(tankCount)
                ).build();
        event.replyModal(modal).queue();
    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        String cmdPrefix = instanceAdminCommand.main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.subcommandGroup + " " + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());
    /*
            String combinedArgs = commandEvent.getArgs();
            String instanceName = combinedArgs.substring(combinedArgs.indexOf("\"") + 1, combinedArgs.lastIndexOf("\""));
            String[] instanceDetails = combinedArgs.substring(combinedArgs.lastIndexOf("\"") +1).trim().split(" ");

            String ShortCode = instanceDetails[0];
            InstanceType Type = null;
            try{
                Type = InstanceType.valueOf(instanceDetails[1].toUpperCase());
            }catch (IllegalArgumentException e){
                commandEvent.replyError("Invalid Type!");
                return;
            }
            String DDCountStr = instanceDetails[2];

            if(instanceAdminCommand.db.getInstanceByShortCode(ShortCode, Type) == null){


                    String HealerCountStr = instanceDetails[3];

                    String TankCountStr = instanceDetails[4];


                    if (!(DDCountStr.matches("(?i)(d:)\\d+")) || !(HealerCountStr.matches("(?i)(h:)\\d+")) || !(TankCountStr.matches("(?i)(t:)\\d+"))) {
                        commandEvent.replyError("Invalid usage!");
                        return;
                    }


                    DDCountInt = Integer.valueOf(DDCountStr.split(":")[1]);
                    HealerCountInt = Integer.valueOf(HealerCountStr.split(":")[1]);
                    TankCountInt = Integer.valueOf(TankCountStr.split(":")[1]);


                    EmbedBuilder eb = new EmbedBuilder();
                    eb.setDescription("Please confirm the below details: ");
                    eb.setTitle("New Instance Request");
                    eb.addField("Instance Name:", instanceName, false);
                    eb.addField("Instance ShortCode:", ShortCode, false);
                    eb.addField("Instance Type:", Type.toString(), false);

                    StringBuilder sb = new StringBuilder();
                    sb.append("\u2694\uFE0F: " + String.valueOf(DDCountInt) + "\n");
                    sb.append("\uD83D\uDC96: " + String.valueOf(HealerCountInt) + "\n");
                    sb.append("\uD83D\uDEE1: " + String.valueOf(TankCountInt) + "\n");

                    eb.addField("Roles:", sb.toString(), false);

                InstanceType finalType = Type;
                waitForConfirmation(commandEvent, eb.build(), () -> instanceAdminCommand.db.createNewInstance(instanceName, ShortCode, finalType, DDCountInt, HealerCountInt, TankCountInt, commandEvent.getAuthor().getId()));
                } else {
                commandEvent.replyError("Duplicate Short-Code");
            }

     */
    }


    private void waitForConfirmation(CommandEvent event, MessageEmbed message, Runnable confirm){

      /*  CustomButtonMenu.Builder mb =  new CustomButtonMenu.Builder()
                .setUsers(event.getAuthor())
                .setEventWaiter(eventAdminCommand.main.eventWaiter)
                .setTimeout(1, TimeUnit.MINUTES)
                .setEmbed(message)
                .setChoices(CONFIRM, CANCEL)
                .setAction(re ->{
                    if(re.getName().equals(CONFIRM)) {
                        confirm.run();
                    }
                    else if(re.getName().equals(CANCEL)) {
                    }
                })
                .setFinalAction(fa -> {
                    fa.delete().queue();
                });

        mb.build().display(event.getChannel());*/
    }
}
