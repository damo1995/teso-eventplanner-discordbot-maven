package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.exceptions.ErrorHandler;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.requests.ErrorResponse;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SubSummonCommand extends SlashCommand {

    EventAdminCommand eventAdminCommand;

    public SubSummonCommand(EventAdminCommand instance){
        this.eventAdminCommand = instance;
        this.name = "summon";
        this.arguments = "<Arg1> <Arg2> ect";
        this.help = "Summons users for a registered event";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.category = new Category("Event Administration");
        this.guildOnly = true;
        this.options = Arrays.asList(
                new OptionData(OptionType.STRING,"messageid", "The message ID of the event to Summon users for").setRequired(true),
                new OptionData(OptionType.STRING,"summonmessage","Optional message to add to the summon PM"));

    }

    private static final Logger logger = LoggerFactory.getLogger(SubSummonCommand.class);

    @Override
    protected void execute(SlashCommandEvent commandEvent) {
        String messageEventId = commandEvent.optString("messageid");
        String summoneMessage = commandEvent.optString("summonmessage");

        if(eventAdminCommand.main.db.isEventMessage(messageEventId)) {
            commandEvent.deferReply(true).queue();

            Event event = eventAdminCommand.main.db.getEventByMessageId(messageEventId);
            List<String> signUps = Stream.of(event.getDds(),event.getHealers(),event.getTanks()).flatMap(Collection::stream).collect(Collectors.toList());

            MessageEmbed embed =  eventAdminCommand.main.utils.generateSummonMessageEmbed(commandEvent.getUser(), event, summoneMessage);
            List<User> failedUsers = eventAdminCommand.main.utils.summonUsers(signUps,embed);

            StringBuilder sb = new StringBuilder();
            if(failedUsers.size() > 0){
                sb.append("We had some issues summoning the below users master.. \n");
                for (User u: failedUsers) {
                    sb.append(u.getName() + "\n");
                }
            }else{
                sb.append("All event Sign-ups have been summoned master.");
            }
            commandEvent.getHook().sendMessage(sb.toString()).setEphemeral(true).queue();
        }else{
            commandEvent.reply("Message ID specified was not a BodgeBot Event!").setEphemeral(true).queue();
        }
    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        String[] items = commandEvent.getArgs().split("\\s+");

        String messageEventId = items[0];
        String summoneMessage = null;

        if (eventAdminCommand.main.db.isEventMessage(messageEventId)){
            Event event = eventAdminCommand.main.db.getEventByMessageId(messageEventId);
            List<String> signUps = Stream.of(event.getDds(),event.getHealers(),event.getTanks()).flatMap(Collection::stream).collect(Collectors.toList());

            if (items.length > 1) {
                summoneMessage = String.join(" ", Arrays.copyOfRange(items, 1, items.length));
            }
            MessageEmbed embed =  eventAdminCommand.main.utils.generateSummonMessageEmbed(commandEvent.getAuthor(), event, summoneMessage);
            for (String signup : signUps){
                logger.debug("ID: " + signup);
                try {
                    eventAdminCommand.main.jda.openPrivateChannelById(signup).queue(msg -> msg.sendMessage(MessageCreateData.fromEmbeds(embed)).queue(), t -> {
                        t.printStackTrace();
                    });
                }catch (ErrorResponseException e){
                    e.printStackTrace();
                    logger.debug(e.getResponse().toString());
                }
            }
        }

    }
}
