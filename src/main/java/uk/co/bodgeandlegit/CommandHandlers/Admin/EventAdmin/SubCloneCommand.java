package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Event;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class SubCloneCommand extends SlashCommand {

    private final EventAdminCommand eventAdminCommand;

    private final Logger logger = LoggerFactory.getLogger(SubCloneCommand.class);

    public SubCloneCommand(EventAdminCommand instance){
        this.eventAdminCommand = instance;

        this.name = "clone";
        this.arguments = "<messageid> <date> <time> <description>";
        this.help = "clones the specified event";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
        this.guildOnly = true;
        this.category = new Category("Event Administration");
        this.options = Arrays.asList(
                new OptionData(OptionType.STRING,"messageid", "The message ID of the event to clone").setRequired(true),
                new OptionData(OptionType.STRING, "date", "Event Date dd/mm/yyyy format").setRequired(true),
                new OptionData(OptionType.STRING, "time", "Event time hh:mm format").setRequired(true),
                new OptionData(OptionType.STRING, "description", "Optional description about the event.").setRequired(false)
        );
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {
        commandEvent.deferReply().queue();

        String messageEventId = commandEvent.optString("messageid");
        logger.info("Requested to Delete Message ID: " + messageEventId);

        Event existingEvent = eventAdminCommand.main.db.getEventByMessageId(messageEventId);


        if(existingEvent != null){
            String eventDate = commandEvent.optString("date");
            String eventTime = commandEvent.optString("time");
            String eventDateTimeStr;
            LocalDateTime dateTime = null;

            DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

            if (eventDate.equalsIgnoreCase("today")) {
                eventDateTimeStr = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + " " + eventTime;
            } else {
                eventDateTimeStr = eventDate + " " + eventTime;
            }

            try {
                dateTime = LocalDateTime.parse(eventDateTimeStr, dtFormatter);
            } catch (Exception e) {
                logger.error("Error whilst trying to convert time/date: " + eventDateTimeStr);
                logger.error(e.toString());
                commandEvent.getHook().sendMessage("Invalid date or Time Specified, Please ensure you use dd/mm/yyyy and hh:mm");
            }

            String description = commandEvent.optString("description");

            EmbedBuilder embedBuilder = eventAdminCommand.main.utils.generateEmbedForEvent(existingEvent, dateTime,description,commandEvent.getMember().getAsMention());
            //EmbedBuilder embedBuilder = main.utils.generateBlankMessage(esoInstanceModel, dateTime, description, commandEvent.getMember().getAsMention());

            String finalDescription = description;
            MessageCreateData msg = new MessageCreateBuilder().addContent("@everyone").addEmbeds(embedBuilder.build()).build();

            LocalDateTime finalDateTime = dateTime;

            commandEvent.getHook().sendMessage(msg).queue(message -> {
                //Shield
                message.addReaction(Emoji.fromUnicode("\uD83D\uDEE1")).queue();
                //Heart
                message.addReaction(Emoji.fromUnicode("\uD83D\uDC96")).queue();
                //Swords
                message.addReaction(Emoji.fromUnicode("\u2694\uFE0F")).queue();
                //Mechs
                message.addReaction(Emoji.fromUnicode("\u2753")).queue();
                //Cross
                message.addReaction(Emoji.fromUnicode("\u274C")).queue();

                eventAdminCommand.main.db.cloneEvent(message.getId(),commandEvent.getChannel().getId(),existingEvent, commandEvent.getMember().getId(), finalDescription, finalDateTime);
            });

        }else{
            commandEvent.reply("Message ID specified was not a BodgeBot Event!").setEphemeral(true).queue();
        }

    }

}
