package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.entities.MessageHistory;

public class SubDeleteHistoryCommand extends SlashCommand {

    EventAdminCommand eventAdminCommand;

    public SubDeleteHistoryCommand(EventAdminCommand instance) {
        this.eventAdminCommand = instance;
        this.name = "cleanup-messages";
        this.help = "clean's up non-bot messages";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.category = new Category("Event Administration");

    }

    @Override
    protected void execute(SlashCommandEvent event) {

    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        if (commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getPledgeChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getTrialChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getDungeonChannelId())) {

            MessageHistory history = commandEvent.getChannel().getHistoryBefore(commandEvent.getChannel().getLatestMessageId(), 100).complete();
            history.getRetrievedHistory().forEach(message -> {
                if(!message.getAuthor().getId().equals(eventAdminCommand.main.jda.getSelfUser().getId())){
                    message.delete().queue();
                }
            });
            commandEvent.getMessage().delete().queue();

        }
    }
}
