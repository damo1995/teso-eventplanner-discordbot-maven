package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.CommandHandlers.Admin.BotAdmin.SubBotStatusCommand;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Main;

public class EventAdminCommand extends SlashCommand {

    public Main main;

    private Logger logger = LoggerFactory.getLogger(EventAdminCommand.class);

    public EventAdminCommand(Main instance){
        this.main = instance;

        this.name = "eventadmin";
        this.requiredRole = main.config.getAdminRole();
        this.category = new Category("Event Administration");
        this.arguments = "<Sub-Command> <Arguments>";
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
        this.guildOnly = true;
        this.children = new SlashCommand[]{
                new SubSignInCommand(this),
                new SubSignOutCommand(this),
                new SubDeleteCommand(this),
                new SubRegenMessageCommand(this),
                //new SubDeleteHistoryCommand(this),
                new SubSummonCommand(this),
                new SubCloneCommand(this)
        };

    }

    @Override
    protected void execute(SlashCommandEvent event) {

    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        EmbedBuilder embed = new EmbedBuilder()
                .setTitle("EventAdmin")
                .setDescription("Here are a list of sub-commands!\n\n`!eventadmin <sub-command>`")
                .setColor(0xB8E0FF);

        for(Command command : getChildren())
        {
            embed.addField(command.getName() + " - " + command.getHelp(), "`" + main.config.getCmdPrefix() + "eventadmin " + command.getName() + " " + command.getArguments() + "`", false);
        }

        commandEvent.replyInDm(embed.build());
        commandEvent.getMessage().delete().queue();

    }
}
