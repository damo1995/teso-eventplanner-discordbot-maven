package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubSignOutCommand extends SlashCommand {

    EventAdminCommand eventAdminCommand;

    public SubSignOutCommand(EventAdminCommand instance){
        this.eventAdminCommand = instance;
        this.name = "signout";
        this.arguments = "<messageid> <atmention user/s>";
        this.help = "Checks out User/s from an event";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.category = new Category("Event Administration");
        this.options = Arrays.asList(
                new OptionData(OptionType.STRING,"messageid", "The message ID of the event to sign users out of").setRequired(true),
                new OptionData(OptionType.STRING,"users","Users to Add to the role").setRequired(true));
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {
        String messageEventId = commandEvent.optString("messageid");

        if(eventAdminCommand.main.db.isEventMessage(messageEventId)) {
            commandEvent.deferReply(true).queue();
            List<User> mentionedUsers = commandEvent.getOption("users").getMentions().getUsers();
            List<User> removedUsers = new ArrayList<>();
            List<User> missingUsers = new ArrayList<>();

            mentionedUsers.forEach(user -> {
                if (eventAdminCommand.main.db.isMemberSignedUp(messageEventId, user.getId())) {
                    removedUsers.add(user);
                    eventAdminCommand.main.db.checkOutUser(messageEventId, user.getId(),commandEvent.getChannel().getId());
                } else {
                    missingUsers.add(user);
                }
            });

            EmbedBuilder eb = new EmbedBuilder().setTitle("Sign in Result");
            StringBuilder sb = new StringBuilder();

            if(removedUsers.size() > 0 ) {
                sb.append("\u2705 **Signed out the following users** \u2705\n");
                for (User u : removedUsers) {
                    sb.append(u.getAsMention() + "\n");
                }
            }
            sb.append("\n");
            if(missingUsers.size() >0){
                sb.append("\u274C **You mentioned the following users, but they weren't signed in** \u274C\n");
                for (User u: missingUsers) {
                    sb.append(u.getAsMention() + "\n");
                }
            }
            eb.setDescription(sb.toString());
            commandEvent.getHook().sendMessage(MessageCreateData.fromEmbeds(eb.build())).queue();
        }else{
            commandEvent.reply("Message ID specified was not a BodgeBot Event!").setEphemeral(true).queue();
        }
    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        if (commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getPledgeChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getTrialChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getDungeonChannelId())) {
            String[] items = commandEvent.getArgs().split("\\s+");

            String messageEventId = items[0];

            if (eventAdminCommand.main.db.isEventMessage(messageEventId)){

                if (commandEvent.getMessage().getMentions().getUsers().size() > 0) {
                    List<User> mentionedUsers = commandEvent.getMessage().getMentions().getUsers();
                    mentionedUsers.forEach(user -> {
                        if (eventAdminCommand.main.db.isMemberSignedUp(messageEventId, user.getId())) {
                            eventAdminCommand.main.db.checkOutUser(messageEventId, user.getId(),commandEvent.getChannel().getId());
                            /*JSONObject auditLogEntry = new JSONObject();
                            auditLogEntry.put("messageId", messageEventId);
                            auditLogEntry.put("user", user.getName());
                            auditLogEntry.put("userId", user.getId());
                            eventAdminCommand.db.insertAuditLogEntry(commandEvent.getMember().getUser().getName(), commandEvent.getMember().getId(), "admin-signout",auditLogEntry.toJSONString());*/
                        } else {
                            commandEvent.replyInDm("User: " + user.getName() + " is not signed up for " + messageEventId);
                        }
                    });
                } else {
                    commandEvent.replyInDm("No users were specified!");
                }

            }else{
                commandEvent.replyInDm("Invalid MessageId Specified!");
            }

            commandEvent.getMessage().delete().queue();
        }
    }
}
