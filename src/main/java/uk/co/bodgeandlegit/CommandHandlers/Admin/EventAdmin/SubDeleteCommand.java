package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class SubDeleteCommand extends SlashCommand {

    private final EventAdminCommand eventAdminCommand;

    private final Logger logger = LoggerFactory.getLogger(SubDeleteCommand.class);

    public SubDeleteCommand(EventAdminCommand instance){
        this.eventAdminCommand = instance;

        this.name = "delete";
        this.arguments = "<messageid>";
        this.help = "Deletes the specified event";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
        this.guildOnly = true;
        this.category = new Category("Event Administration");
        this.options = Arrays.asList(new OptionData(OptionType.STRING,"messageid", "The message ID of the event to delete").setRequired(true));
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {
        String messageEventId = commandEvent.optString("messageid");
        logger.info("Requested to Delete Message ID: " + messageEventId);

        if(eventAdminCommand.main.db.isEventMessage(messageEventId)){
            eventAdminCommand.main.db.deleteEvent(messageEventId, commandEvent.getMember());
            commandEvent.reply("Event Deleted!").setEphemeral(true).queue();
        }else{
            commandEvent.reply("Message ID specified was not a BodgeBot Event!").setEphemeral(true).queue();
        }

    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        if (commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getPledgeChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getTrialChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getDungeonChannelId())) {

            String[] items = commandEvent.getArgs().split("\\s+");
            String messageEventId = items[0];
            logger.info("Requested to Delete Message ID: " + messageEventId);

            if (eventAdminCommand.main.db.isEventMessage(messageEventId)){
                eventAdminCommand.main.db.deleteEvent(messageEventId, commandEvent.getMember());
            }else{
                commandEvent.replyInDm("Invalid MessageId specified!");
            }

        }
        commandEvent.getMessage().delete().queue();
    }
}
