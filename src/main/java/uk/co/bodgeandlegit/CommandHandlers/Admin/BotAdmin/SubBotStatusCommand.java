package uk.co.bodgeandlegit.CommandHandlers.Admin.BotAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class SubBotStatusCommand extends SlashCommand {

    BotAdminCommand botAdminCommand;
    private Logger logger = LoggerFactory.getLogger(SubBotStatusCommand.class);

    public SubBotStatusCommand(BotAdminCommand instance){
        this.botAdminCommand = instance;
        this.guildOnly = true;
        this.name = "set-status";
        this.arguments = "<listening/competing/playing/watching/reset> <activity>";
        this.help = "Sets the bot's current activity";
        this.requiredRole = botAdminCommand.main.config.getAdminRole();
        this.category = new Category("Bot Administration");
        OptionData od = new OptionData(OptionType.STRING, "activity","Activity Type").setRequired(true);
        od.addChoice("listening", "listening")
          .addChoice("competing", "competing")
          .addChoice("playing", "playing")
          .addChoice("watching", "watching")
          .addChoice("reset", "reset");
        this.options = Arrays.asList(od, new OptionData(OptionType.STRING,"description", "the description to append to the activity").setRequired(true));


    }

    @Override
    protected void execute(SlashCommandEvent event) {
        String description = event.optString("description");

        switch (event.optString("activity")){
            case "listening":
                logger.debug("listening");
                botAdminCommand.main.jda.getPresence().setActivity(Activity.listening(description));
                break;
            case "competing":
                logger.debug("competing");
                botAdminCommand.main.jda.getPresence().setActivity(Activity.competing(description));
                break;
            case "playing":
                logger.debug("playing");
                botAdminCommand.main.jda.getPresence().setActivity(Activity.playing(description));
                break;
            case "watching":
                logger.debug("watching");
                botAdminCommand.main.jda.getPresence().setActivity(Activity.watching(description));
                break;
            case "reset":
                logger.debug("reset");
                botAdminCommand.main.jda.getPresence().setActivity(Activity.listening("your Event Requests!"));
                break;
        }
        event.reply("Updated the bot status!").setEphemeral(true).queue();
    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        String cmdPrefix = botAdminCommand.main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.subcommandGroup + " " + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());
    }
}
