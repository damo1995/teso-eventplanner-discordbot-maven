package uk.co.bodgeandlegit.CommandHandlers.Admin.BotAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Main;

public class BotAdminCommand extends SlashCommand {

    public Main main;
    public DataAccessLayer db;

    private Logger logger = LoggerFactory.getLogger(BotAdminCommand.class);

    public BotAdminCommand(Main instance){
        this.main = instance;
        this.db = main.db;

        this.name = "botadmin";
        this.requiredRole = main.config.getAdminRole();
        this.category = new Category("Bot Administration");
        this.arguments = "<Sub-Command> <Arguments>";
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
        this.children = new SlashCommand[]{new SubBotStatusCommand(this)};
        this.guildOnly = true;

    }

    @Override
    protected void execute(SlashCommandEvent event) {

    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        String cmdPrefix = main.config.getCmdPrefix();
        StringBuilder sb = new StringBuilder();
        sb.append("The command ");
        sb.append(cmdPrefix + this.name);
        if(this.aliases.length > 0 ){
            for (String alias: this.aliases) {
                sb.append(", " + cmdPrefix + alias);
            }
        }
        sb.append(" has been replaced by the Slash Command /" + this.name);
        sb.append("  Please use this instead.");
        commandEvent.getMessage().delete().queue();
        commandEvent.replyInDm(sb.toString());

    }
}
