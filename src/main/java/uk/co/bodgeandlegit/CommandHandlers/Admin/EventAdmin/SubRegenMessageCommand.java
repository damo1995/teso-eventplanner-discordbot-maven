package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.command.SlashCommand;
import com.jagrosh.jdautilities.command.SlashCommandEvent;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Event;

import java.util.Arrays;

public class SubRegenMessageCommand extends SlashCommand {

    EventAdminCommand eventAdminCommand;

    public SubRegenMessageCommand(EventAdminCommand instance) {
        this.eventAdminCommand = instance;
        this.name = "regen-message";
        this.arguments = "<messageid>";
        this.help = "Regenerate an event message";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.userPermissions = new Permission[]{Permission.ADMINISTRATOR};
        this.guildOnly = true;
        this.category = new Category("Event Administration");
        this.options = Arrays.asList(new OptionData(OptionType.STRING,"messageid", "The ID of the Message to Regenerate").setRequired(true));
    }

    @Override
    protected void execute(SlashCommandEvent commandEvent) {
        String messageEventId = commandEvent.optString("messageid");

        if(eventAdminCommand.main.db.isEventMessage(messageEventId)){
            Event event = eventAdminCommand.main.db.getEventByMessageId(messageEventId);
            eventAdminCommand.main.utils.regenerateMessage(messageEventId,event.getChannelId());
            commandEvent.reply("Regenerated requested message!").setEphemeral(true).queue();
        }else {
            commandEvent.reply("Message ID specified was not a BodgeBot Event!").setEphemeral(true).queue();
        }
    }

    @Override
    protected void execute(CommandEvent commandEvent) {
        String[] items = commandEvent.getArgs().split("\\s+");

        String messageEventId = items[0];

        if (commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getPledgeChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getTrialChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getDungeonChannelId())) {
            eventAdminCommand.main.utils.regenerateMessage(messageEventId, commandEvent.getChannel().getId());
            commandEvent.getMessage().delete().queue();
        }
    }
}
