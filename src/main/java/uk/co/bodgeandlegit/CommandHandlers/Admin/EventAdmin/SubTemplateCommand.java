package uk.co.bodgeandlegit.CommandHandlers.Admin.EventAdmin;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

public class SubTemplateCommand extends Command {

    EventAdminCommand eventAdminCommand;

    public SubTemplateCommand(EventAdminCommand instance){
        this.eventAdminCommand = instance;
        this.name = "command-name";
        this.arguments = "<Arg1> <Arg2> ect";
        this.help = "Command Description";
        this.requiredRole = eventAdminCommand.main.config.getAdminRole();
        this.category = new Category("Event Administration");
    }

    @Override
    protected void execute(CommandEvent commandEvent) {

        if (commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getPledgeChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getTrialChannelId()) || commandEvent.getChannel().getId().equals(eventAdminCommand.main.config.getDungeonChannelId())) {

        }
    }
}
