package uk.co.bodgeandlegit.Database.Model.MongoDB;

public class EventReserve {
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    private String role;

    public static final class EventReserveBuilder {
        private String userId;
        private String role;

        public EventReserveBuilder() {
        }

        public static EventReserveBuilder anEventReserve() {
            return new EventReserveBuilder();
        }

        public EventReserveBuilder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public EventReserveBuilder withRole(String role) {
            this.role = role;
            return this;
        }

        public EventReserve build() {
            EventReserve eventReserve = new EventReserve();
            eventReserve.setUserId(userId);
            eventReserve.setRole(role);
            return eventReserve;
        }
    }
}
