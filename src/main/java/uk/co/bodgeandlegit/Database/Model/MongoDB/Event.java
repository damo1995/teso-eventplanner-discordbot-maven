package uk.co.bodgeandlegit.Database.Model.MongoDB;

import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.time.LocalDateTime;
import java.util.List;

public class Event {

    private ObjectId id;
    private String messageId;
    private String channelId;
    private String instanceId;
    private String organiserId;
    private String description;
    @BsonProperty(value = "event_date")
    private LocalDateTime eventDate;
    private List<String> tanks;
    private List<String> healers;
    private List<String> dds;
    private List<EventReserve> reserves;
    private List<String> mechs;


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getInstanceId() { return instanceId; }

    public void setInstanceId(String instanceId) { this.instanceId = instanceId; }

    public String getOrganiserId() {
        return organiserId;
    }

    public void setOrganiserId(String organiserId) {
        this.organiserId = organiserId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(LocalDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public List<String> getTanks() {
        return tanks;
    }

    public void setTanks(List<String> tanks) {
        this.tanks = tanks;
    }

    public List<String> getHealers() {
        return healers;
    }

    public void setHealers(List<String> healers) {
        this.healers = healers;
    }

    public List<String> getDds() {
        return dds;
    }

    public void setDds(List<String> dds) {
        this.dds = dds;
    }

    public List<EventReserve> getReserves() {
        return reserves;
    }

    public void setReserves(List<EventReserve> reserves) {
        this.reserves = reserves;
    }

    public List<String> getMechs(){return mechs;}

    public void setMechs(List<String> mechs){this.mechs = mechs;}

    public static final class EventBuilder {
        private ObjectId id;
        private String messageId;
        private String channelId;
        private String instanceId;
        private String organiserId;
        private String description;
        private LocalDateTime eventDate;
        private List<String> tanks;
        private List<String> healers;
        private List<String> dds;
        private List<EventReserve> reserves;
        private List<String> mechs;

        public EventBuilder() {
        }

        public static EventBuilder anEvent() {
            return new EventBuilder();
        }

        public EventBuilder withId(ObjectId id) {
            this.id = id;
            return this;
        }

        public EventBuilder withMessageId(String messageId) {
            this.messageId = messageId;
            return this;
        }

        public EventBuilder withChannelId(String channelId) {
            this.channelId = channelId;
            return this;
        }

        public EventBuilder withInstanceId(String instanceId) {
            this.instanceId = instanceId;
            return this;
        }

        public EventBuilder withOrganiserId(String organiserId) {
            this.organiserId = organiserId;
            return this;
        }

        public EventBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public EventBuilder withEventDate(LocalDateTime eventDate) {
            this.eventDate = eventDate;
            return this;
        }

        public EventBuilder withTanks(List<String> tanks) {
            this.tanks = tanks;
            return this;
        }

        public EventBuilder withHealers(List<String> healers) {
            this.healers = healers;
            return this;
        }

        public EventBuilder withDds(List<String> dds) {
            this.dds = dds;
            return this;
        }

        public EventBuilder withReserves(List<EventReserve> reserves) {
            this.reserves = reserves;
            return this;
        }

        public EventBuilder withMechs(List<String> mechs){
            this.mechs = mechs;
            return this;
        }

        public Event build() {
            Event event = new Event();
            event.setId(id);
            event.setMessageId(messageId);
            event.setChannelId(channelId);
            event.setInstanceId(instanceId);
            event.setOrganiserId(organiserId);
            event.setDescription(description);
            event.setEventDate(eventDate);
            event.setTanks(tanks);
            event.setHealers(healers);
            event.setDds(dds);
            event.setReserves(reserves);
            event.setMechs(mechs);
            return event;
        }
    }
}
/*
{
    "_id": {
        "$oid": "613cf52dab688ad8a550c79b"
    },
    "messageId": "123456789",
    "instanceId": "613ad189",
    "organiserId": "9876543210",
    "description": "Test Description",
    "event_date": {
        "$date": "2021-01-01T15:00:00.000Z"
    },
    "tanks": ["123456789", "987654321"],
    "healers": ["123456789", "123456789"],
    "dds": ["123456798", "123456789", "123456789", "123456789", "123456798", "123459789", "123456789", "123456798"],
    "reserves": [{
        "userId": "123456798",
        "role": "tank"
    }, {
        "userId": "123456789",
        "role": "dd"
    }]
}
 */