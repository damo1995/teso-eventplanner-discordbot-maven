package uk.co.bodgeandlegit.Database.Model.MongoDB;

import org.bson.types.ObjectId;

public class AuditLogEntry {
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Event getEventData() {
        return EventData;
    }

    public void setEventData(Event eventData) {
        EventData = eventData;
    }

    private ObjectId id;
    private String action;
    private String user;
    private Event EventData;

    public static final class AuditLogEntryBuilder {
        private ObjectId id;
        private String action;
        private String user;
        private Event EventData;

        public AuditLogEntryBuilder() {
        }

        public static AuditLogEntryBuilder anAuditLogEntry() {
            return new AuditLogEntryBuilder();
        }

        public AuditLogEntryBuilder withId(ObjectId id) {
            this.id = id;
            return this;
        }

        public AuditLogEntryBuilder withAction(String action) {
            this.action = action;
            return this;
        }

        public AuditLogEntryBuilder withUser(String user) {
            this.user = user;
            return this;
        }

        public AuditLogEntryBuilder withEventData(Event EventData) {
            this.EventData = EventData;
            return this;
        }

        public AuditLogEntry build() {
            AuditLogEntry auditLogEntry = new AuditLogEntry();
            auditLogEntry.setId(id);
            auditLogEntry.setAction(action);
            auditLogEntry.setUser(user);
            auditLogEntry.setEventData(EventData);
            return auditLogEntry;
        }
    }
}
