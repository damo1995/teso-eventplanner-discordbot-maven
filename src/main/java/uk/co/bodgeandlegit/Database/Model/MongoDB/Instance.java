package uk.co.bodgeandlegit.Database.Model.MongoDB;

import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;
import uk.co.bodgeandlegit.Utils.InstanceType;

public class Instance {
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public InstanceType getType() {
        return type;
    }

    public void setType(InstanceType type) {
        this.type = type;
    }

    public int getRoleDDCount() {
        return roleDDCount;
    }

    public void setRoleDDCount(int roleDDCount) {
        this.roleDDCount = roleDDCount;
    }

    public int getRoleHealerCount() {
        return roleHealerCount;
    }

    public void setRoleHealerCount(int roleHealerCount) {
        this.roleHealerCount = roleHealerCount;
    }

    public int getRoleTankCount() {
        return roleTankCount;
    }

    public void setRoleTankCount(int roleTankCount) {
        this.roleTankCount = roleTankCount;
    }

    private ObjectId id;
    private String name;
    private String shortname;
    private InstanceType type;
    @BsonProperty(value = "role_dd_count")
    private int roleDDCount;
    @BsonProperty(value = "role_healer_count")
    private int roleHealerCount;
    @BsonProperty(value = "role_tank_count")
    private int roleTankCount;


    public static final class InstanceBuilder {
        private String name;
        private String shortname;
        private InstanceType type;
        private int roleDDCount;
        private int roleHealerCount;
        private int roleTankCount;

        public InstanceBuilder() {
        }

        public static InstanceBuilder anInstance() {
            return new InstanceBuilder();
        }

        public InstanceBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public InstanceBuilder withShortname(String shortname) {
            this.shortname = shortname;
            return this;
        }

        public InstanceBuilder withType(InstanceType type) {
            this.type = type;
            return this;
        }

        public InstanceBuilder withRoleDDCount(int roleDDCount) {
            this.roleDDCount = roleDDCount;
            return this;
        }

        public InstanceBuilder withRoleHealerCount(int roleHealerCount) {
            this.roleHealerCount = roleHealerCount;
            return this;
        }

        public InstanceBuilder withRoleTankCount(int roleTankCount) {
            this.roleTankCount = roleTankCount;
            return this;
        }

        public Instance build() {
            Instance instance = new Instance();
            instance.setName(name);
            instance.setShortname(shortname);
            instance.setType(type);
            instance.setRoleDDCount(roleDDCount);
            instance.setRoleHealerCount(roleHealerCount);
            instance.setRoleTankCount(roleTankCount);
            return instance;
        }
    }
}
/*
{
    "_id": {
        "$oid": "613cf1c3ab688ad8a550c6e9"
    },
    "name": "Pledge Run (Normal)",
    "shortname": "normal",
    "type": "pledge",
    "role_dd_count": 2,
    "role_healer_count": 1,
    "role_tank_count": 1
}
 */
