package uk.co.bodgeandlegit.Database;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertOneResult;
import com.mongodb.client.result.UpdateResult;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.Model.MongoDB.AuditLogEntry;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Event;
import uk.co.bodgeandlegit.Database.Model.MongoDB.EventReserve;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Instance;
import uk.co.bodgeandlegit.Main;
import uk.co.bodgeandlegit.Utils.InstanceType;
import uk.co.bodgeandlegit.Utils.RoleType;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.mongodb.client.model.Filters.eq;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class DataAccessLayer {

    private final Main main;
    private static final Logger logger = LoggerFactory.getLogger(DataAccessLayer.class);

    public MongoDatabase db;

    public List<Instance> instanceList = new ArrayList<>();
    public List<Event> eventList = new ArrayList<>();

    private MongoCollection<Instance> instanceMongoCollection;
    private MongoCollection<Event> eventMongoCollection;
    private MongoCollection<AuditLogEntry> auditLogEntryMongoCollection;


    public DataAccessLayer(Main main, String host, String database, String username, String password){

        this.main = main;

        CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
        CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
        MongoClientSettings clientSettings = MongoClientSettings.builder()
                .applyConnectionString(new ConnectionString(String.format("mongodb://%s:%s@%s",username, password,host)))
                .codecRegistry(codecRegistry)
                .build();
        try{
            MongoClient mongoClient = MongoClients.create(clientSettings);
            db = mongoClient.getDatabase(database);
            instanceMongoCollection = db.getCollection("instances", Instance.class);
            eventMongoCollection = db.getCollection("events", Event.class);
            auditLogEntryMongoCollection = db.getCollection("auditlog", AuditLogEntry.class);

            populateInstanceList();
            populateEventList();

        }catch (Exception e){
            logger.error("We had an issue connecting to the MongoDB Database! Please see the below: ");
            logger.error(e.toString());
            System.exit(0);
        }
    }

    public boolean isEventMessage(String messageId){

        try {
            Event evt =  eventList.stream().filter(event -> messageId.equals(event.getMessageId())).findFirst().orElse(null);
            return  Objects.nonNull(evt);

        }catch (Exception e){
            logger.error("Error checking Message Id: " + messageId);
            logger.error(e.toString());
            return false;
        }
    }

    public boolean isMemberSignedUp(String messageId, String userId){

        try {
            Event evt =  eventList.stream().filter(event -> messageId.equals(event.getMessageId())).findFirst().orElse(null);

            if(evt != null){

                EventReserve reserve = evt.getReserves().stream().filter(eventReserve -> userId.equals(eventReserve.getUserId())).findFirst().orElse(null);

                return evt.getDds().contains(userId) || evt.getHealers().contains(userId) || evt.getTanks().contains(userId) || reserve != null;

            }
            return false;
        } catch (Exception e) {
            logger.error("Error checking signup for User: " + userId + "for Message: " + messageId);
            logger.error(e.toString());
            return false;
        }
}

    public EventReserve getNextReserve(String eventId, String role){

        Event event = getEventByMessageId(eventId);

        return event.getReserves().stream().filter(eventReserve -> role.equals(eventReserve.getRole())).findFirst().orElse(null);
    }

    public Instance getInstanceByShortCode(String shortcode, InstanceType type){

        try {
            return instanceList.stream().filter(insta -> type.equals(insta.getType())).filter(insta -> shortcode.equals(insta.getShortname())).findFirst().orElse(null);
        }catch (Exception e){
            logger.warn("Error querying for shortcode: " + shortcode);
            logger.warn(e.toString());
            return null;
        }

    }

    public Instance getInstanceByMessageId(String messageId){

        try {
            Event event = eventList.stream().filter(evt -> messageId.equals(evt.getMessageId())).findFirst().orElse(null);
            if(event != null){
                Instance instance = instanceList.stream().filter(insta -> event.getInstanceId().equals(insta.getId().toString())).findFirst().orElse(null);
                if(instance != null){
                    return instance;
                }
            }
        }catch (Exception e){
            logger.error("Error whilst getting instance for MessageId: " + messageId);
            logger.error(e.toString());
        }
        return null;
    }

    public Instance getInstanceByInstanceId(String instanceId){

        try {
            Instance instance = instanceList.stream().filter(inst -> instanceId.equals(inst.getId().toString())).findFirst().orElse(null);
            if(instance != null){
                return instance;
            }
        }catch (Exception e){
            logger.error("Error whilst getting instance for instanceId: " + instanceId);
            logger.error(e.toString());
        }
        return null;
    }

    public List<Instance> getInstances(InstanceType type){
        return  instanceList.stream().filter(insta -> type.equals(insta.getType())).sorted(Comparator.comparing(Instance::getName)).collect(Collectors.toList());
    }

    public Event getEventByMessageId(String messageId){
        try{

            return eventList.stream().filter(evt -> messageId.equals(evt.getMessageId())).findFirst().orElse(null);

        }catch (Exception e){
            logger.error("Error whilst getting event for MessageId: "+ messageId);
            logger.error(e.toString());
            return null;
        }
    }

    public void populateInstanceList(){
        instanceList.clear();
        instanceMongoCollection.find().into(instanceList);
    }

    public void populateEventList(){
        eventList.clear();
        eventMongoCollection.find().into(eventList);
    }

    public boolean createNewEvent(String messageId, String channelId, Instance instance, String organiser, String description, LocalDateTime eventDate){

        try{

            Event event = new Event.EventBuilder()
                    .withMessageId(messageId)
                    .withChannelId(channelId)
                    .withInstanceId(instance.getId().toString())
                    .withOrganiserId(organiser)
                    .withDescription(description)
                    .withEventDate(eventDate)
                    .withTanks(new ArrayList<String>())
                    .withHealers(new ArrayList<String>())
                    .withDds(new ArrayList<String>())
                    .withReserves(new ArrayList<EventReserve>())
                    .withMechs(new ArrayList<String>())
                    .build();

            InsertOneResult result = eventMongoCollection.insertOne(event);

            if(result.wasAcknowledged()) {
                event.setId(result.getInsertedId().asObjectId().getValue());
                eventList.add(event);
                auditLogEntryMongoCollection.insertOne(new AuditLogEntry.AuditLogEntryBuilder().withAction("create-event").withUser(organiser).withEventData(event).build());
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean cloneEvent(String messageId, String channelId, Event existingEvent, String organiser, String description, LocalDateTime eventDate){

        try{

            Event event = new Event.EventBuilder()
                    .withMessageId(messageId)
                    .withChannelId(channelId)
                    .withInstanceId(existingEvent.getInstanceId())
                    .withOrganiserId(organiser)
                    .withDescription(description)
                    .withEventDate(eventDate)
                    .withTanks(existingEvent.getTanks())
                    .withHealers(existingEvent.getHealers())
                    .withDds(existingEvent.getDds())
                    .withReserves(existingEvent.getReserves())
                    .withMechs(existingEvent.getMechs())
                    .build();

            InsertOneResult result = eventMongoCollection.insertOne(event);

            if(result.wasAcknowledged()) {
                event.setId(result.getInsertedId().asObjectId().getValue());
                eventList.add(event);
                auditLogEntryMongoCollection.insertOne(new AuditLogEntry.AuditLogEntryBuilder().withAction("create-event").withUser(organiser).withEventData(event).build());
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean createNewInstance(String instanceName, String ShortCode, InstanceType Type, int DDCount, int HealerCount, int TankCount, String SpawnedUser){

        try{
            Instance instance = new Instance.InstanceBuilder()
                    .withName(instanceName)
                    .withShortname(ShortCode)
                    .withType(Type)
                    .withRoleDDCount(DDCount)
                    .withRoleHealerCount(HealerCount)
                    .withRoleTankCount(TankCount)
                    .build();

            logger.debug("Instance Name: " + instance.getName());
            logger.debug("Short Code: " + instance.getShortname());
            logger.debug("Instance Type: " + instance.getType());
            logger.debug("~~~~~~~~~~~~~~~~~~~~~");
            logger.debug("DD Count INT: " + instance.getRoleDDCount());
            logger.debug("HEALER Count INT: " + instance.getRoleHealerCount());
            logger.debug("TANK Count INT: " + instance.getRoleTankCount());

            InsertOneResult result = instanceMongoCollection.insertOne(instance);

            if(result.wasAcknowledged()) {
                instance.setId(result.getInsertedId().asObjectId().getValue());
                instanceList.add(instance);
                //auditLogEntryMongoCollection.insertOne(new AuditLogEntry.AuditLogEntryBuilder().withAction("create-instance").withUser(SpawnedUser).withEventData(instance).build());
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateInstance(Instance instance){

        try{

            logger.debug("Instance Name: " + instance.getName());
            logger.debug("Short Code: " + instance.getShortname());
            logger.debug("Instance Type: " + instance.getType());
            logger.debug("~~~~~~~~~~~~~~~~~~~~~");
            logger.debug("DD Count INT: " + instance.getRoleDDCount());
            logger.debug("HEALER Count INT: " + instance.getRoleHealerCount());
            logger.debug("TANK Count INT: " + instance.getRoleTankCount());

            //InsertOneResult result = instanceMongoCollection.insertOne(instance);
            UpdateResult result =  instanceMongoCollection.replaceOne(eq("_id", instance.getId()),instance);
            //eventMongoCollection.replaceOne(eq("_id", event.getId()), event);

            if(result.wasAcknowledged()) {
                //instance.setId(result.getInsertedId().asObjectId().getValue());
                //instanceList.add(instance);
                //auditLogEntryMongoCollection.insertOne(new AuditLogEntry.AuditLogEntryBuilder().withAction("create-instance").withUser(SpawnedUser).withEventData(instance).build());
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public void checkInUser(String messageId, String userId, RoleType role, String channelId){

        logger.debug("Entered 'CheckInUser'");

        try {

            int eventIX = IntStream.range(0, eventList.size()).filter(evt -> eventList.get(evt).getMessageId().equals(messageId)).findFirst().getAsInt();

            Event event = eventList.get(eventIX);

            Instance instance = instanceList.stream().filter(insta -> event.getInstanceId().equals(insta.getId().toString())).findFirst().orElse(null);

            int tankCount = event.getTanks().size();
            int healerCount = event.getHealers().size();
            int ddCount = event.getDds().size();

            switch (role) {

                case TANK:
                    if (tankCount < instance.getRoleTankCount()) {
                        event.getTanks().add(userId);
                        logger.debug("TANK IS NOT RESERVE!!");
                        break;
                    }else{
                        event.getReserves().add(new EventReserve.EventReserveBuilder().withRole("tank").withUserId(userId).build());
                        logger.debug("TANK IS RESERVE!!");
                        break;
                    }
                case HEALER:
                    if (healerCount < instance.getRoleHealerCount()) {
                        event.getHealers().add(userId);
                        logger.debug("HEALER IS NOT RESERVE !!!");
                        break;
                    }else{
                        event.getReserves().add(new EventReserve.EventReserveBuilder().withRole("healer").withUserId(userId).build());
                        logger.debug("HEALER IS RESERVE!!");
                        break;
                    }
                case DD:
                    if (ddCount < instance.getRoleDDCount()) {
                        event.getDds().add(userId);
                        logger.debug("DD IS NOT RESERVE!!!");
                        break;
                    }else{
                        event.getReserves().add(new EventReserve.EventReserveBuilder().withRole("dd").withUserId(userId).build());
                        logger.debug("DD IS RESERVE!!");
                        break;
                    }
            }
            eventList.set(eventIX, event);

            eventMongoCollection.replaceOne(eq("_id", event.getId()), event);

            main.utils.regenerateMessage(messageId, channelId);

        }catch (Exception e){
            logger.error("Error checking in user: " + userId + " for event: " + messageId);
            logger.error(e.toString());
        }
    }

    public void checkOutUser(String messageId, String userID, String channelId){

        try{
            RoleType role = null;
            int eventIX = IntStream.range(0, eventList.size()).filter(evt -> eventList.get(evt).getMessageId().equals(messageId)).findFirst().getAsInt();
            Event event = getEventByMessageId(messageId);
            if(event.getTanks().contains(userID)){
                role = RoleType.TANK;
                event.getTanks().remove(userID);
                EventReserve reserve = getNextReserve(messageId, role.toString().toLowerCase());
                if(reserve != null){
                    event.getTanks().add(reserve.getUserId());
                    event.getReserves().remove(reserve);
                    main.utils.sendReserveStateChangePrivateMessage(reserve.getUserId(), RoleType.TANK, event);
                }
                if(event.getMechs().contains(userID)){
                    event.getMechs().remove(userID);
                }
                eventList.set(eventIX, event);
                eventMongoCollection.replaceOne(eq("_id", event.getId()), event);

            }else if(event.getHealers().contains(userID)){
                role = RoleType.HEALER;
                event.getHealers().remove(userID);
                EventReserve reserve = getNextReserve(messageId, role.toString().toLowerCase());
                if(reserve != null){
                    event.getHealers().add(reserve.getUserId());
                    event.getReserves().remove(reserve);
                    main.utils.sendReserveStateChangePrivateMessage(reserve.getUserId(), RoleType.HEALER, event);
                }
                if(event.getMechs().contains(userID)){
                    event.getMechs().remove(userID);
                }
                eventList.set(eventIX, event);
                eventMongoCollection.replaceOne(eq("_id", event.getId()), event);
            }else if(event.getDds().contains(userID)){
                role = RoleType.DD;
                event.getDds().remove(userID);
                EventReserve reserve = getNextReserve(messageId, role.toString().toLowerCase());
                if(reserve != null){
                    event.getDds().add(reserve.getUserId());
                    event.getReserves().remove(reserve);
                    main.utils.sendReserveStateChangePrivateMessage(reserve.getUserId(), RoleType.DD, event);
                }
                if(event.getMechs().contains(userID)){
                    event.getMechs().remove(userID);
                }
                eventList.set(eventIX, event);
                eventMongoCollection.replaceOne(eq("_id", event.getId()), event);
            }else{
                EventReserve reserve = event.getReserves().stream().filter(eventReserve -> userID.equals(eventReserve.getUserId())).findFirst().orElse(null);
                if( reserve != null){
                    event.getReserves().remove(reserve);
                    if(event.getMechs().contains(reserve.getUserId())){
                        event.getMechs().remove(reserve.getUserId());
                    }
                    eventList.set(eventIX, event);
                    eventMongoCollection.replaceOne(eq("_id", event.getId()), event);
                }
            }
                main.utils.regenerateMessage(messageId, channelId);
        }
        catch (Exception e){
            logger.error("Error checking out user: " + userID + " for messageId: " + messageId);
            logger.error(e.toString());
        }
    }

    public void deleteEvent(String messageId, Member cause){

        Event event = getEventByMessageId(messageId);

        if(event != null){

            eventList.remove(event);
            eventMongoCollection.deleteOne(eq("_id", event.getId()));

           /* JSONArray users = new JSONArray();

            JSONObject auditLogEntry = new JSONObject();
            auditLogEntry.put("messageId", messageId);
            auditLogEntry.put("instanceId", event.getInstance().getId());
            auditLogEntry.put("description",event.getDescription());
            auditLogEntry.put("datetime",event.getEvent_datetime());
            auditLogEntry.put("organiser",event.getOrganiser());
            auditLogEntry.put("signups", users);

            try {
                eventsDao.delete(event);
            }catch (SQLException e){
                logger.error("Error deleting event: " + event.getMessageid());
                logger.error(e.toString());
            }
            this.insertAuditLogEntry(cause.getUser().getName(), cause.getId(), "delete-event",auditLogEntry.toJSONString());*/
            Message msg = main.jda.getTextChannelById(event.getChannelId()).retrieveMessageById(messageId).complete();
            msg.delete().queue();
            auditLogEntryMongoCollection.insertOne(new AuditLogEntry.AuditLogEntryBuilder().withAction("delete-event").withUser(cause.getId()).withEventData(event).build());
        }

    }

    public void flagRequiresMech(String messageId, String userId, String channelId){

        logger.debug("Entered 'flagRequiresMech'");

        try {

            int eventIX = IntStream.range(0, eventList.size()).filter(evt -> eventList.get(evt).getMessageId().equals(messageId)).findFirst().getAsInt();

            Event event = eventList.get(eventIX);


            if(event.getMechs().contains(userId)){
                event.getMechs().remove(userId);
            }else {
                event.getMechs().add(userId);
            }

            eventList.set(eventIX, event);

            eventMongoCollection.replaceOne(eq("_id", event.getId()), event);

            main.utils.regenerateMessage(messageId, channelId);

        }catch (Exception e){
            logger.error("Error checking in user: " + userId + " for event: " + messageId);
            logger.error(e.toString());
        }
    }
    
}

