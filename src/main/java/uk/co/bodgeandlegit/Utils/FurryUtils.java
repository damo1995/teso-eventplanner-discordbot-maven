package uk.co.bodgeandlegit.Utils;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import uk.co.bodgeandlegit.Config;

import java.awt.*;
import java.util.NoSuchElementException;
import java.util.Random;

public class FurryUtils {

    private static String catAPI = "https://api.thecatapi.com/v1/images/search";
    private static String dogAPI = "https://dog.ceo/api/breeds/image/random";
    private static String duckAPI = "https://random-d.uk/api/random";

    private static String foxAPI = "https://randomfox.ca/floof/";

    private static String bunnyAPI = "https://api.bunnies.io/v2/loop/random/?media=gif,png";
    private static String unsplashApiRandomImage = "https://api.unsplash.com/photos/random";
    private static OkHttpClient webClient = new OkHttpClient();
    private static Random rand = new Random();

    private Config config;

    public FurryUtils(Config config) {
        this.config = config;
    }

    public MessageEmbed getRandomPussy(){
        EmbedBuilder eb = new EmbedBuilder();

        try {

            Request.Builder request = new Request.Builder();
            request.url(catAPI);
            request.addHeader("x-api-key", config.getTheCatAPIKey());

            Response resp = webClient.newCall(request.build()).execute();
            if(resp.code() == 200){
                JSONArray jsonArray = new JSONArray(resp.body().string());
                JSONObject jsonobject = (JSONObject) jsonArray.get(0);
                eb.setImage((String) jsonobject.get("url"));
                eb.setColor(new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat()));
                return eb.build();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public MessageEmbed getRandomDoggo(){
        EmbedBuilder eb = new EmbedBuilder();

        try {

            Request.Builder request = new Request.Builder();
            request.url(dogAPI);

            Response resp = webClient.newCall(request.build()).execute();
            if(resp.code() == 200){

                JSONObject jsonobject = new JSONObject(resp.body().string());
                eb.setImage((String) jsonobject.get("message"));
                eb.setColor(new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat()));
                return eb.build();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public MessageEmbed getRandomDuck(){
        EmbedBuilder eb = new EmbedBuilder();

        try {

            Request.Builder request = new Request.Builder();
            request.url(duckAPI);

            Response resp = webClient.newCall(request.build()).execute();
            if(resp.code() == 200){

                JSONObject jsonobject = new JSONObject(resp.body().string());
                eb.setImage((String) jsonobject.get("url"));
                eb.setColor(new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat()));
                return eb.build();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public MessageEmbed getRandomFox(){
        EmbedBuilder eb = new EmbedBuilder();

        try {

            Request.Builder request = new Request.Builder();
            request.url(foxAPI);

            Response resp = webClient.newCall(request.build()).execute();
            if(resp.code() == 200){

                JSONObject jsonobject = new JSONObject(resp.body().string());
                eb.setImage((String) jsonobject.get("image"));
                eb.setColor(new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat()));
                return eb.build();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public MessageEmbed getRandomPig(){
        EmbedBuilder eb = new EmbedBuilder();

        try {

            Request.Builder request = new Request.Builder();
            request.addHeader("Authorization", "Client-ID " +  config.getUnsplashAPIKey());

            HttpUrl.Builder urlBuilder = HttpUrl.parse(unsplashApiRandomImage).newBuilder();
            urlBuilder.addQueryParameter("collections", "3378453,48866498,3275683,2237487,1242250,10559417,RKtLKCD6xYg,1542786,3816635");

            request.url(urlBuilder.build());


            Response resp = webClient.newCall(request.build()).execute();
            if(resp.code() == 200){

                JSONObject jsonobject = new JSONObject(resp.body().string());

                eb.setImage((String) jsonobject.getJSONObject("urls").get("regular"));
                eb.setColor(new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat()));
                return eb.build();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public MessageEmbed getRandomBunny(){
        EmbedBuilder eb = new EmbedBuilder();

        try {

            Request.Builder request = new Request.Builder();
            request.url(bunnyAPI);

            Response resp = webClient.newCall(request.build()).execute();
            if(resp.code() == 200){

                JSONObject jsonobject = new JSONObject(resp.body().string()).getJSONObject("media");

                eb.setImage(jsonobject.get("gif").toString());
                eb.setColor(new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat()));
                return eb.build();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public MessageEmbed getRandomImage(String searchTerm){
        EmbedBuilder eb = new EmbedBuilder();

        try {

            Request.Builder request = new Request.Builder();
            request.addHeader("Authorization", "Client-ID " +  config.getUnsplashAPIKey());

            HttpUrl.Builder urlBuilder = HttpUrl.parse(unsplashApiRandomImage).newBuilder();

            if(searchTerm != null) {
                urlBuilder.addQueryParameter("query", searchTerm);
            }

            request.url(urlBuilder.build());


            Response resp = webClient.newCall(request.build()).execute();
            if(resp.code() == 404){
                throw new NoSuchElementException();
            }
            else if(resp.code() == 200){

                JSONObject jsonobject = new JSONObject(resp.body().string());
                String imageSourceUrl = jsonobject.getJSONObject("urls").getString("regular") + "?utm_source=BodgeBot&utm_medium=referral";
                String imageUrl = jsonobject.getJSONObject("links").getString("html");
                String authorName = jsonobject.getJSONObject("user").getString("name");
                String authorLink = jsonobject.getJSONObject("user").getJSONObject("links").getString("html");
                String description = String.format("Photo by [%s](%s?utm_source=BodgeBot&utm_medium=referral) on [Unsplash](https://unsplash.com/?utm_source=BodgeBot&utm_medium=referral)", authorName, authorLink);
                eb.setDescription(description);
                eb.setFooter("Photo URL: " + imageUrl);

                eb.setImage(imageSourceUrl);
                eb.setColor(new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat()));
                return eb.build();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


}
