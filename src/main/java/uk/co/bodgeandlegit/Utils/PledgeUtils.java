package uk.co.bodgeandlegit.Utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PledgeUtils {

    private static ArrayList<String> MAJ = new ArrayList<>(Arrays.asList("Fungal Grotto I", "The Banished Cells II", "Darkshade Caverns I", "Elden Hollow II", "Wayrest Sewers I", "Spindleclutch II", "Banished Cells I", "Fungal Grotto II", "Spindleclutch I", "Darkshade Caverns II", "Elden Hollow I", "Wayrest Sewers II"));
    private static ArrayList<String> GLIRION = new ArrayList<>(Arrays.asList("Selene's Web", "City of Ash II", "Crypt Of Hearts I", "Volenfell", "Blessed Crucible", "Direfrost Keep", "Vaults of Madness", "Crypt of Hearts II", "City of Ash I", "Tempest Island", "Blackheart Haven", "Arx Corinium"));
    private static ArrayList<String> URGARLAG = new ArrayList<>(Arrays.asList("Moongrave Fane","Lair of Maarselok","Icereach","Unhallowed Grave","Stone Garden","Castle Thorn","Black Drake Villa","The Cauldron","Imperial City Prison","Ruins of Mazzatun","White-Gold Tower","Cradle of Shadows","Bloodroot Forge","Falkreath Hold","Fang Lair","Scalecaller Peak","Moon Hunter Keep","March of Sacrifices","Depths of Malatar","Frostvault"));

    private static int ORIGIN_TIMESTAMP = 1502690400;
    private static int MAJ_AND_GLIRION_CYCLE = 12;
    private static int URGARLAG_CYCLE = 20;


    public static List<String> getCSVPledgeStringForDate(LocalDateTime ts){

        /*  --------------------------------------------------------------- */
        long secondsSinceStart = ts.atZone(ZoneId.of("Europe/London")).toEpochSecond();
        /*  --------------------------------------------------------------- */

        BigDecimal bigDecimal  = BigDecimal.valueOf(secondsSinceStart / 86400.0);
        double daysElapsed = bigDecimal.intValue();

        BigDecimal MG = BigDecimal.valueOf(daysElapsed/MAJ_AND_GLIRION_CYCLE);
        double cycleFractionMG = MG.subtract(BigDecimal.valueOf(MG.intValue())).doubleValue();
        int daysSinceCycleStartMajAndGlirion = round(cycleFractionMG*MAJ_AND_GLIRION_CYCLE);

        BigDecimal U = BigDecimal.valueOf(daysElapsed/URGARLAG_CYCLE);
        double cycleFractionU = U.subtract(BigDecimal.valueOf(U.intValue())).doubleValue();
        int daysSinceCycleStartUrgarlag = round(cycleFractionU*URGARLAG_CYCLE);
        List<String> pledges = Arrays.asList(MAJ.get(daysSinceCycleStartMajAndGlirion), GLIRION.get(daysSinceCycleStartMajAndGlirion), URGARLAG.get(daysSinceCycleStartUrgarlag));
        return pledges;
    }

    public static int round(Double val){
        return (int) Math.floor(val+0.5);
    }


}
