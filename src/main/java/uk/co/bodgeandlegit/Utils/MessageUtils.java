package uk.co.bodgeandlegit.Utils;

import com.jagrosh.jdautilities.commons.utils.TableBuilder;
import com.jagrosh.jdautilities.menu.Paginator;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.exceptions.ErrorHandler;
import net.dv8tion.jda.api.requests.ErrorResponse;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import net.dv8tion.jda.api.utils.messages.MessageEditData;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.bodgeandlegit.Database.DataAccessLayer;
import uk.co.bodgeandlegit.Main;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Event;
import uk.co.bodgeandlegit.Database.Model.MongoDB.EventReserve;
import uk.co.bodgeandlegit.Database.Model.MongoDB.Instance;

import java.awt.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class MessageUtils {

    private final DataAccessLayer db;
    private final Main main;

    private static final Logger logger = LoggerFactory.getLogger(MessageUtils.class);

    public MessageUtils(Main instance){
        this.main = instance;
        this.db = main.db;
    }

    String rowDelimiter = "\u2500";
    String columnDelimiter= "\u2502";
    String crossDelimiter = "\u253C";
    String leftIntersection = "\u251C";
    String rightIntersection = "\u2524";
    String upperIntersection=  "\u252C";
    String lowerIntersection=  "\u2534";
    String upLeftCorner= "\u250C";
    String upRightCorner= "\u2510";
    String lowLeftCorner= "\u2514";
    String lowRightCorner= "\u2518";
    String headerDelimiter= "\u2550";
    String headerCrossDelimiter="\u256A";
    String headerLeftIntersection= "\u255E";
    String headerRightIntersection="\u2561";
    String firstColumnDelimiter="\u2551";
    String firstColumnCrossDelimiter="\u256B";
    String firstColumnUpperIntersection="\u2565";
    String firstColumnLowerIntersection="\u2568";
    String headerColumnCrossDelimiter="\u256C";
    String horizontalOutline="\u2500";
    String verticalOutline="\u2502";

    private final TableBuilder.Borders bordersToUse = TableBuilder.Borders.newHeaderRowNamesFrameBorders(rowDelimiter, columnDelimiter, crossDelimiter, leftIntersection, rightIntersection, upperIntersection, lowerIntersection, upLeftCorner, upRightCorner, lowLeftCorner, lowRightCorner, headerDelimiter, headerCrossDelimiter, headerLeftIntersection, headerRightIntersection, firstColumnDelimiter, firstColumnCrossDelimiter, firstColumnUpperIntersection, firstColumnLowerIntersection, headerColumnCrossDelimiter, horizontalOutline, verticalOutline);

    public EmbedBuilder generateBlankMessage(Instance instanceModel, LocalDateTime dateTime, String description, String owner){


        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(new Color(4886754));

        /*embedBuilder.setTitle(instanceModel.getName() +
                              " run is planned for " +
                                DateTimeFormatter.ofPattern("dd/MM EEEE").format(dateTime) +
                              " at " +
                                DateTimeFormatter.ofPattern("HH:mm").format(dateTime)*/

        embedBuilder.setTitle(instanceModel.getName() +
                              " run is planned for <t:"+ dateTime.atZone(ZoneId.of("Europe/London")).toEpochSecond() +">"
        );

        //Prepend description if we have one, Else lets just add the Owner.
        if(description != null) {
            embedBuilder.setDescription(description + "\n \uD83D\uDC51: " + owner);
        }else{
            embedBuilder.setDescription("\uD83D\uDC51: " + owner);
        }

        if(instanceModel.getType() == InstanceType.PLEDGE){
            PledgeUtils.getCSVPledgeStringForDate(dateTime).forEach(pledge -> embedBuilder.appendDescription("\n<:UndauntedKey:843177844024803389>: " + pledge));
        }

        /*  Lets build our blank canvas
            I think there's an artist hidden at the bottom of every single one of us. - Bob Ross
         */
        StringBuilder rosterSB = new StringBuilder();
        for (int i = 0; i<instanceModel.getRoleTankCount(); i++) { rosterSB.append("\uD83D\uDEE1:\n"); }
        for (int i = 0; i<instanceModel.getRoleHealerCount(); i++) { rosterSB.append("\uD83D\uDC96:\n"); }
        for (int i = 0; i< instanceModel.getRoleDDCount(); i++) { rosterSB.append("\u2694\uFE0F:\n"); }

        embedBuilder.addField("Roster", rosterSB.toString(), false);
        embedBuilder.addField("React with:",
                                    "\uD83D\uDEE1 to sign in as tank\n" +
                                        "\uD83D\uDC96 to sign in as healer\n" +
                                        "\u2694\uFE0F to sign in as DD\n" +
                                        "\u2753 if you require mech explanations\n" +
                                        "\u274C to sign out",
                false);

        return embedBuilder;

    }

    public EmbedBuilder generateEmbedForEvent(Event event, LocalDateTime dateTime, String description, String owner){

        Instance instanceModel = db.getInstanceByInstanceId(event.getInstanceId());
        EmbedBuilder embedBuilder;

        if(event.getTanks().size() == 0 && event.getDds().size() == 0 && event.getHealers().size() == 0 && event.getReserves().size() == 0){
            embedBuilder =  this.generateBlankMessage(instanceModel, dateTime, description, owner);
        }else{

            embedBuilder = new EmbedBuilder();
            embedBuilder.setColor(new Color(4886754));
            embedBuilder.setTitle(instanceModel.getName() +
                    " run is planned for <t:"+ dateTime.atZone(ZoneId.of("Europe/London")).toEpochSecond() +">"
            );

            //Prepend description if we have one, Else lets just add the Owner.

            if(description != null) {
                embedBuilder.setDescription(description + "\n \uD83D\uDC51: " + owner);
            }else{
                logger.debug("Event Organiser: " + event.getOrganiserId());
                embedBuilder.setDescription("\uD83D\uDC51: " + owner);
            }

            if(instanceModel.getType() == InstanceType.PLEDGE){
                PledgeUtils.getCSVPledgeStringForDate(dateTime).forEach(pledge -> {
                    embedBuilder.appendDescription("\n<:UndauntedKey:843177844024803389>: " + pledge);
                });
            }


            StringBuilder rosterSB = new StringBuilder();
            StringBuilder reserveSB = new StringBuilder();

            List<String> tankSignups = event.getTanks();
            List<String> healzSignups = event.getHealers();
            List<String> ddSignups = event.getDds();
            List<EventReserve> reserves = event.getReserves();
            List<String> requiresMechs = event.getMechs();


            int tankDeficit = instanceModel.getRoleTankCount() - tankSignups.size();
            int healzDeficit = instanceModel.getRoleHealerCount() - healzSignups.size();
            int ddDeficit = instanceModel.getRoleDDCount() - ddSignups.size();

            if(tankSignups.size() > 0){
                for (String signUp: tankSignups) {

                    rosterSB.append("\uD83D\uDEE1:" + main.jda.retrieveUserById(signUp).complete().getAsMention() + (requiresMechs.contains(signUp) ? " \u2753":"") + "\n");
                }
            }
            for (int i = 0; i<tankDeficit; i++) { rosterSB.append("\uD83D\uDEE1:\n"); }


            if(healzSignups.size() > 0){
                for (String signUp: healzSignups) {
                    rosterSB.append("\uD83D\uDC96:" + main.jda.retrieveUserById(signUp).complete().getAsMention() + (requiresMechs.contains(signUp) ? " \u2753":"") + "\n");
                }
            }
            for (int i = 0; i<healzDeficit; i++) { rosterSB.append("\uD83D\uDC96:\n"); }



            if(ddSignups.size() > 0){
                for (String signUp: ddSignups) {
                    rosterSB.append("\u2694\uFE0F:" + main.jda.retrieveUserById(signUp).complete().getAsMention() + (requiresMechs.contains(signUp) ? " \u2753":"") + "\n");
                }
            }
            for (int i = 0; i<ddDeficit; i++) { rosterSB.append("\u2694\uFE0F:\n"); }

            if(reserves.size() > 0){
                for (EventReserve signUp: reserves) {
                    if(signUp.getRole().equals("tank")){
                        reserveSB.append("\uD83D\uDEE1:" + main.jda.retrieveUserById(signUp.getUserId()).complete().getAsMention() + (requiresMechs.contains(signUp.getUserId()) ? " \u2753":"") + "\n");
                    }
                    if(signUp.getRole().equals("healer")){
                        reserveSB.append("\uD83D\uDC96:" + main.jda.retrieveUserById(signUp.getUserId()).complete().getAsMention() + (requiresMechs.contains(signUp.getUserId()) ? " \u2753":"") + "\n");
                    }
                    if(signUp.getRole().equals("dd")){
                        reserveSB.append("\u2694\uFE0F:" + main.jda.retrieveUserById(signUp.getUserId()).complete().getAsMention() + (requiresMechs.contains(signUp.getUserId()) ? " \u2753":"") + "\n");
                    }
                }
            }

            embedBuilder.addField("Roster", rosterSB.toString(), false);
            if(reserves.size() >0){
                embedBuilder.addField("Reserves:", reserveSB.toString(), false);
            }

            embedBuilder.addField("React with:",
                    "\uD83D\uDEE1 to sign in as tank\n" +
                            "\uD83D\uDC96 to sign in as healer\n" +
                            "\u2694\uFE0F to sign in as DD\n" +
                            "\u2753 if you require mech explanations\n" +
                            "\u274C to sign out",
                    false);
        }
        return embedBuilder;
    }

    public void regenerateMessage(String messageId, String channelId){

        Instance instanceModel = db.getInstanceByMessageId(messageId);

        Event event = db.getEventByMessageId(messageId);

        EmbedBuilder embedBuilder;

        if(event.getTanks().size() == 0 && event.getDds().size() == 0 && event.getHealers().size() == 0 && event.getReserves().size() == 0){
            embedBuilder = this.generateBlankMessage(instanceModel, event.getEventDate(), event.getDescription(),  main.jda.retrieveUserById(event.getOrganiserId()).complete().getAsMention());

            Message msg = main.jda.getTextChannelById(channelId).retrieveMessageById(messageId).complete();

            MessageEditData data = MessageEditData.fromContent("@everyone").fromEmbeds(embedBuilder.build());

            msg.editMessage(data).queue();
            //Shield
            msg.addReaction(Emoji.fromUnicode("\uD83D\uDEE1")).queue();
            //Heart
            msg.addReaction(Emoji.fromUnicode("\uD83D\uDC96")).queue();
            //Swords
            msg.addReaction(Emoji.fromUnicode("\u2694\uFE0F")).queue();
            //Mechs
            msg.addReaction(Emoji.fromUnicode("\u2753")).queue();
            //Cross
            msg.addReaction(Emoji.fromUnicode("\u274C")).queue();

        }
        else {
            embedBuilder = new EmbedBuilder();
            embedBuilder.setColor(new Color(4886754));
            embedBuilder.setTitle(instanceModel.getName() +
                    " run is planned for <t:"+ event.getEventDate().atZone(ZoneId.of("Europe/London")).toEpochSecond() +">"
            );

            //Prepend description if we have one, Else lets just add the Owner.
            User organiser = main.jda.retrieveUserById(event.getOrganiserId()).complete();
            if(event.getDescription() != null) {
                embedBuilder.setDescription(event.getDescription() + "\n \uD83D\uDC51: " + organiser.getAsMention());
            }else{
                logger.debug("Event Organiser: " + event.getOrganiserId());
                embedBuilder.setDescription("\uD83D\uDC51: " + organiser.getAsMention());
            }

            if(instanceModel.getType() == InstanceType.PLEDGE){
                PledgeUtils.getCSVPledgeStringForDate(event.getEventDate()).forEach(pledge -> {
                    embedBuilder.appendDescription("\n<:UndauntedKey:843177844024803389>: " + pledge);
                });
            }


            StringBuilder rosterSB = new StringBuilder();
            StringBuilder reserveSB = new StringBuilder();

            List<String> tankSignups = event.getTanks();
            List<String> healzSignups = event.getHealers();
            List<String> ddSignups = event.getDds();
            List<EventReserve> reserves = event.getReserves();
            List<String> requiresMechs = event.getMechs();


            int tankDeficit = instanceModel.getRoleTankCount() - tankSignups.size();
            int healzDeficit = instanceModel.getRoleHealerCount() - healzSignups.size();
            int ddDeficit = instanceModel.getRoleDDCount() - ddSignups.size();

            if(tankSignups.size() > 0){
                for (String signUp: tankSignups) {

                    rosterSB.append("\uD83D\uDEE1:" + main.jda.retrieveUserById(signUp).complete().getAsMention() + (requiresMechs.contains(signUp) ? " \u2753":"") + "\n");
                }
            }
            for (int i = 0; i<tankDeficit; i++) { rosterSB.append("\uD83D\uDEE1:\n"); }


            if(healzSignups.size() > 0){
                for (String signUp: healzSignups) {
                    rosterSB.append("\uD83D\uDC96:" + main.jda.retrieveUserById(signUp).complete().getAsMention() + (requiresMechs.contains(signUp) ? " \u2753":"") + "\n");
                }
            }
            for (int i = 0; i<healzDeficit; i++) { rosterSB.append("\uD83D\uDC96:\n"); }



            if(ddSignups.size() > 0){
                for (String signUp: ddSignups) {
                    rosterSB.append("\u2694\uFE0F:" + main.jda.retrieveUserById(signUp).complete().getAsMention() + (requiresMechs.contains(signUp) ? " \u2753":"") + "\n");
                }
            }
            for (int i = 0; i<ddDeficit; i++) { rosterSB.append("\u2694\uFE0F:\n"); }

            if(reserves.size() > 0){
                for (EventReserve signUp: reserves) {
                    if(signUp.getRole().equals("tank")){
                        reserveSB.append("\uD83D\uDEE1:" + main.jda.retrieveUserById(signUp.getUserId()).complete().getAsMention() + (requiresMechs.contains(signUp.getUserId()) ? " \u2753":"") + "\n");
                    }
                    if(signUp.getRole().equals("healer")){
                        reserveSB.append("\uD83D\uDC96:" + main.jda.retrieveUserById(signUp.getUserId()).complete().getAsMention() + (requiresMechs.contains(signUp.getUserId()) ? " \u2753":"") + "\n");
                    }
                    if(signUp.getRole().equals("dd")){
                        reserveSB.append("\u2694\uFE0F:" + main.jda.retrieveUserById(signUp.getUserId()).complete().getAsMention() + (requiresMechs.contains(signUp.getUserId()) ? " \u2753":"") + "\n");
                    }
                }
            }

            embedBuilder.addField("Roster", rosterSB.toString(), false);
            if(reserves.size() >0){
                embedBuilder.addField("Reserves:", reserveSB.toString(), false);
            }

            embedBuilder.addField("React with:",
                    "\uD83D\uDEE1 to sign in as tank\n" +
                            "\uD83D\uDC96 to sign in as healer\n" +
                            "\u2694\uFE0F to sign in as DD\n" +
                            "\u2753 if you require mech explanations\n" +
                            "\u274C to sign out",
                    false);

            logger.info("Message Id: " + messageId + " Channel Id: " + channelId);
            Message msg = main.jda.getTextChannelById(channelId).retrieveMessageById(messageId).complete();
            MessageEditData data = MessageEditData.fromContent("@everyone").fromEmbeds(embedBuilder.build());
            msg.editMessage(data).queue();
            //Shield
            msg.addReaction(Emoji.fromUnicode("\uD83D\uDEE1")).queue();
            //Heart
            msg.addReaction(Emoji.fromUnicode("\uD83D\uDC96")).queue();
            //Swords
            msg.addReaction(Emoji.fromUnicode("\u2694\uFE0F")).queue();
            //Mechs
            msg.addReaction(Emoji.fromUnicode("\u2753")).queue();
            //Cross
            msg.addReaction(Emoji.fromUnicode("\u274C")).queue();

        }
    }

    public Paginator.Builder generateKnownInstancesPaged(InstanceType type){

        List<String> tables = new ArrayList<>();
        List<Instance> instances = db.getInstances(type);
        List<List<Instance>> instancesInChunks = ListUtils.partition(instances, 5);

        for(List<Instance> list : instancesInChunks){
            tables.add(new TableBuilder()
                    .addHeaders("ShortCode","Event Name")
                    .setValues(convertInstancesForList(list))
                    .frame(true)
                    .setBorders(bordersToUse)
                    .codeblock(true)
                    .autoAdjust(true)
                    .build()
            );
        }

        Paginator.Builder paginatorBuilder = new Paginator.Builder()
                .setColumns(1)
                .setText("Known ESO Instances:")
                .showPageNumbers(true)
                .useNumberedItems(false)
                .setColor(new Color(4886754))
                .waitOnSinglePage(true)
                .setItemsPerPage(1)
                .setFinalAction(message -> {
                    try {
                        message.delete().queue();
                    } catch(RejectedExecutionException e) { }
                })
                .setEventWaiter(main.eventWaiter)
                .setTimeout(1, TimeUnit.MINUTES);

        for(String str : tables){
            paginatorBuilder.addItems(str);
        }

        return paginatorBuilder;
    }

    private String[][] convertInstancesForList(List<Instance> instances){
        String[][] instanceArray = new String[instances.size()][2];

        for (int i = 0; i < instances.size(); i++) {
            instanceArray[i][0] = instances.get(i).getShortname();
            instanceArray[i][1] = instances.get(i).getName();
        }
        return instanceArray;
    }

    public void sendReserveStateChangePrivateMessage(String user, RoleType role, Event event){
        Instance instanceModel = db.getInstanceByInstanceId(event.getInstanceId());

        PrivateChannel pc = main.jda.getUserById(user).openPrivateChannel().complete();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(new Color(26,255,0));
        embedBuilder.setThumbnail("https://cdn.discordapp.com/avatars/834915630024097833/4c2f39519394679c08f96c2eb338ae5b.webp?size=256");
        embedBuilder.setTitle(String.format("You're no longer a reserve for the %s event", instanceModel.getName()));
        if(event.getDescription() != null) embedBuilder.setDescription(event.getDescription());
        embedBuilder.addField("Date/Time:",String.format("<t:%s>",event.getEventDate().atZone(ZoneId.of("Europe/London")).toEpochSecond()),true);
        switch (role){
            case TANK:
                embedBuilder.addField("Role:", "\uD83D\uDEE1", true);
                break;
            case HEALER:
                embedBuilder.addField("Role:", "\uD83D\uDC96", true);
                break;
            case DD:
                embedBuilder.addField("Role:", "\u2694\uFE0F", true);
                break;
        }
        embedBuilder.addField("Leader:", main.jda.getUserById(event.getOrganiserId()).getName(), true);
        embedBuilder.addField("","And now for a BodgeFather Joke:",false);
        embedBuilder.setFooter(main.bodgefatherYivo.getRandomDadJoke());
        MessageCreateData data = MessageCreateData.fromEmbeds(embedBuilder.build());
        pc.sendMessage(data).queue();

    }

    public MessageEmbed generateSummonMessageEmbed(User summoner, Event event, String summonMessage){
        Instance instanceModel = db.getInstanceByInstanceId(event.getInstanceId());

        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setColor(new Color(26,255,0));
        embedBuilder.setThumbnail((summoner.getAvatarUrl() == null) ? summoner.getDefaultAvatarUrl() : summoner.getAvatarUrl());
        embedBuilder.setTitle(String.format("You've been summoned by %s for the event %s", summoner.getName(), instanceModel.getName()));
        if(summonMessage != null){
            embedBuilder.setDescription(summonMessage);
        }
        embedBuilder.addField("Date/Time:",String.format("<t:%s>",event.getEventDate().atZone(ZoneId.of("Europe/London")).toEpochSecond()),true);
        embedBuilder.addField("Leader:", main.jda.getUserById(event.getOrganiserId()).getName(), true);
        if(event.getDescription()!=null){embedBuilder.addField("Description:", event.getDescription(), true);}

        return embedBuilder.build();

    }

    public List<User> summonUsers(List<String> usersToSummon, MessageEmbed message){

        List<User> failedUsers = new ArrayList<>();

        for (String signup : usersToSummon) {
            User user = main.jda.retrieveUserById(signup).complete();

            logger.debug(user.toString());
            try {
                user.openPrivateChannel().flatMap(channel -> channel.sendMessage(MessageCreateData.fromEmbeds(message)))
                        .queue(null, new ErrorHandler()
                                .handle(ErrorResponse.CANNOT_SEND_TO_USER,
                                        (ex) -> failedUsers.add(user)));
            }catch (Exception e){
                failedUsers.add(user);
            }

        }
        return failedUsers;
    }

}
