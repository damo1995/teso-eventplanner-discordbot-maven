package uk.co.bodgeandlegit.Utils;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import org.json.JSONObject;
import uk.co.bodgeandlegit.Config;

public class DadJokeUtils {

    private static String jokeAPI = "https://icanhazdadjoke.com/";

    private Config config;

    public DadJokeUtils(Config config) {
        this.config = config;
    }

    private static OkHttpClient webClient = new OkHttpClient();

    public String getRandomDadJoke(){

            try {

                Request.Builder request = new Request.Builder();
                request.url(jokeAPI);
                request.addHeader("Accept", "application/json");

                Response resp = webClient.newCall(request.build()).execute();
                if(resp.code() == 200){

                    JSONObject jsonobject = new JSONObject(resp.body().string());
                    return jsonobject.getString("joke");
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            return null;

    }

}
