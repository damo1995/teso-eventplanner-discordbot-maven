package uk.co.bodgeandlegit;

import lombok.Getter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;


public class Config {

    @Getter private String botToken;
    @Getter private String ownerId;

    @Getter private String cmdPrefix;
    @Getter private String helpCmd;
    @Getter private String dungeonChannelId;
    @Getter private String trialChannelId;
    @Getter private String pledgeChannelId;
    @Getter private String trainingChannelId;


    @Getter private String animalDiscoveryChannelId;

    @Getter private String eventPlannerRole;
    @Getter private String adminRole;

    @Getter private String dbHost;
    @Getter private String dbUsername;
    @Getter private String dbPassword;

    @Getter private String dbName;

    @Getter private String unsplashAPIKey;

    @Getter private String theCatAPIKey;

    private static final Logger logger = LoggerFactory.getLogger(Config.class);

    private static final Map<String, String> systemEnv = System.getenv();

    public Config() {

        if(systemEnv.containsKey("BODGEBOT_USEENV") && systemEnv.get("BODGEBOT_USEENV").equals("1")) {
            logger.info("Using System Environment for config options.");
            logger.info("~~~~~ General Bot Config ~~~~~");

            if(systemEnv.containsKey("BB_BOTTOKEN")) {
                this.botToken = systemEnv.get("BB_BOTTOKEN");
            }else {
                logger.error("BB_BOTTOKEN ENVAR not set!");
                System.exit(0);
            }

            if(systemEnv.containsKey("BB_OWNERID")) {
                this.ownerId = systemEnv.get("BB_OWNERID");
            }else {
                logger.error("BB_OWNERID ENVAR not set!");
                System.exit(0);
            }
            logger.info("OwnerId: " + this.ownerId);

            this.cmdPrefix = systemEnv.getOrDefault("BB_CMDPREFIX", "!");
            logger.info("Command Prefix: " + this.cmdPrefix);

            this.helpCmd = systemEnv.getOrDefault("BB_HELPCMD", "bodgehelp");
            logger.info("Help Command: " + this.helpCmd);

            if(systemEnv.containsKey("BB_EVENTPLANNERROLE")) {
                this.eventPlannerRole = systemEnv.get("BB_EVENTPLANNERROLE");
            }else {
                logger.error("BB_EVENTPLANNERROLE ENVAR not set!");
                System.exit(0);
            }
            logger.info("EventPlanner Role: " + this.eventPlannerRole);

            if(systemEnv.containsKey("BB_ADMINROLE")) {
                this.adminRole = systemEnv.get("BB_ADMINROLE");
            }else {
                logger.error("BB_ADMINROLE ENVAR not set!");
                System.exit(0);
            }
            logger.info("Admin Role: " + this.adminRole);

            logger.info("~~~~~ Channel Config ~~~~~");

            if(systemEnv.containsKey("BB_CHAN_TRIALS")) {
                this.trialChannelId = systemEnv.get("BB_CHAN_TRIALS");
            }else {
                logger.error("BB_CHAN_TRIALS ENVAR not set!");
                System.exit(0);
            }
            logger.info("Trial ChannelID: " + this.trialChannelId);

            if(systemEnv.containsKey("BB_CHAN_DUNGEONS")) {
                this.dungeonChannelId = systemEnv.get("BB_CHAN_DUNGEONS");
            }else {
                logger.error("BB_CHAN_DUNGEONS ENVAR not set!");
                System.exit(0);
            }
            logger.info("Dungeon ChannelID: " + this.dungeonChannelId);

            if(systemEnv.containsKey("BB_CHAN_PLEDGES")) {
                this.pledgeChannelId = systemEnv.get("BB_CHAN_PLEDGES");
            }else {
                logger.error("BB_CHAN_PLEDGES ENVAR not set!");
                System.exit(0);
            }
            logger.info("Pledge ChannelID: " + this.pledgeChannelId);

            if(systemEnv.containsKey("BB_CHAN_TRAINING")) {
                this.pledgeChannelId = systemEnv.get("BB_CHAN_TRAINING");
            }else {
                logger.error("BB_CHAN_TRAINING ENVAR not set!");
                System.exit(0);
            }
            logger.info("Training ChannelID: " + this.trainingChannelId);

            this.animalDiscoveryChannelId = systemEnv.getOrDefault("BB_CHAN_ANIMALDISCOVERYCHANNEL", null);
            logger.info("Animal Discovery Channel ChannelID: " + this.animalDiscoveryChannelId);

            logger.info("~~~~~ Database Config ~~~~~");

            if(systemEnv.containsKey("BB_DB_HOST")) {
                this.dbHost = systemEnv.get("BB_DB_HOST");
            }else {
                logger.error("BB_DB_HOST ENVAR not set!");
                System.exit(0);
            }
            logger.info("Database Host: " + this.dbHost);

            if(systemEnv.containsKey("BB_DB_DATABASE")) {
                this.dbName = systemEnv.get("BB_DB_DATABASE");
            }else {
                logger.error("BB_DB_DATABASE ENVAR not set!");
                System.exit(0);
            }
            logger.info("Database Name: " + this.dbName);

            if(systemEnv.containsKey("BB_DB_USERNAME")) {
                this.dbUsername = systemEnv.get("BB_DB_USERNAME");
            }else {
                logger.error("BB_DB_USERNAME ENVAR not set!");
                System.exit(0);
            }
            logger.info("Database Username: " + this.dbUsername);

            if(systemEnv.containsKey("BB_DB_PASSWORD")) {
                this.dbPassword = systemEnv.get("BB_DB_PASSWORD");
            }else {
                logger.error("BB_DB_PASSWORD ENVAR not set!");
                System.exit(0);
            }

            logger.info("~~~~~ APIKeys Config ~~~~~");

            this.unsplashAPIKey = systemEnv.getOrDefault("BB_API_UNSPLASH", null);
            logger.info("Unsplash API Key: " + this.unsplashAPIKey);

            this.theCatAPIKey = systemEnv.getOrDefault("BB_API_THECATAPI", null);
            logger.info("Cat API Key: " + this.theCatAPIKey);


        }else {
            try {
                File f = new File("config.json");
                logger.info("Checking for config.json Existence");

                if (!f.exists()) {
                    logger.info("Creating Config.json");
                    copy(getClass().getResourceAsStream("/config.json"), getBasePathForClass(Config.class) + "config.json");
                    logger.info("Default config created, Please configure the bot before re-running.");
                    System.exit(0);

                } else {
                    logger.info("Reading Config");
                    FileReader reader = new FileReader("config.json");
                    JSONParser jsonParser = new JSONParser();
                    JSONObject configObject = (JSONObject) jsonParser.parse(reader);

                    logger.info("~~~~~ General Bot Config ~~~~~");

                    this.botToken = configObject.get("botToken").toString();
                    this.ownerId = configObject.get("ownerId").toString();
                    logger.info("OwnerId: " + this.ownerId);

                    this.cmdPrefix = configObject.get("cmdPrefix").toString();
                    logger.info("Command Prefix: " + this.cmdPrefix);

                    this.helpCmd = configObject.get("helpCmd").toString();
                    logger.info("Help Command: " + this.helpCmd);

                    this.eventPlannerRole = configObject.get("eventPlannerRole").toString();
                    logger.info("EventPlanner Role: " + this.eventPlannerRole);

                    this.adminRole = configObject.get("adminRole").toString();
                    logger.info("Admin Role: " + this.adminRole);

                    logger.info("~~~~~ Channel Config ~~~~~");

                    JSONObject channels = (JSONObject) configObject.get("channels");
                    this.dungeonChannelId = channels.get("dungeons").toString();
                    logger.info("Dungeon ChannelID: " + this.dungeonChannelId);

                    this.trialChannelId = channels.get("trials").toString();
                    logger.info("Trial ChannelID: " + this.trialChannelId);

                    this.pledgeChannelId = channels.get("pledges").toString();
                    logger.info("Pledge ChannelID: " + this.pledgeChannelId);

                    this.trainingChannelId = channels.get("training").toString();
                    logger.info("Training ChannelID: " + this.trainingChannelId);

                    this.animalDiscoveryChannelId = channels.get("animaldiscoverychannel").toString();
                    logger.info("Training ChannelID: " + this.trainingChannelId);

                    logger.info("~~~~~ Database Config ~~~~~");

                    JSONObject database = (JSONObject) configObject.get("database");
                    this.dbHost = database.get("host").toString();
                    logger.info("Database Host: " + this.dbHost);

                    this.dbName = database.get("database").toString();
                    logger.info("Database Name: " + this.dbName);
                    this.dbUsername = database.get("username").toString();
                    logger.info("Database Username: " + this.dbUsername);
                    this.dbPassword = database.get("password").toString();

                    logger.info("~~~~~ API Config ~~~~~");

                    JSONObject api = (JSONObject) configObject.get("api");
                    this.unsplashAPIKey = api.get("unsplash").toString();
                    logger.info("Unsplash API Key: " + this.unsplashAPIKey);

                    this.theCatAPIKey = api.get("thecatapi").toString();
                    logger.info("TheCatAPI Key: " + this.theCatAPIKey);

                }

            } catch (IOException | ParseException ex) {
                logger.error(ex.toString());
                ex.printStackTrace();
            }

        }
    }

    public static boolean copy(InputStream source , String destination) {
        boolean succeess = true;

        logger.debug("Copying ->" + source + " to -> " + destination);

        try {
            Files.copy(source, Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.warn(ex.toString());
            succeess = false;
        }

        return succeess;

    }

    public static String getBasePathForClass(Class<?> classs) {

        // Local variables
        File file;
        String basePath = "";
        boolean failed = false;

        // Let's give a first try
        try {
            file = new File(classs.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());

            if (file.isFile() || file.getPath().endsWith(".jar") || file.getPath().endsWith(".zip")) {
                basePath = file.getParent();
            } else {
                basePath = file.getPath();
            }
        } catch (URISyntaxException ex) {
            failed = true;
            logger.warn("Cannot figure out base path for class with way (1): " + ex );
        }

        // The above failed?
        if (failed) {
            try {
                file = new File(classs.getClassLoader().getResource("").toURI().getPath());
                basePath = file.getAbsolutePath();

                // the below is for testing purposes...
                // starts with File.separator?
                // String l = local.replaceFirst("[" + File.separator +
                // "/\\\\]", "")
            } catch (URISyntaxException ex) {
                logger.warn("Cannot figurse out base path for class with way (2): " + ex);
            }
        }

        // fix to run inside eclipse
        if (basePath.endsWith(File.separator + "lib") || basePath.endsWith(File.separator + "bin")
                || basePath.endsWith("bin" + File.separator) || basePath.endsWith("lib" + File.separator)) {
            basePath = basePath.substring(0, basePath.length() - 4);
        }
        // fix to run inside netbeans
        if (basePath.endsWith(File.separator + "build" + File.separator + "classes")) {
            basePath = basePath.substring(0, basePath.length() - 14);
        }
        // end fix
        if (!basePath.endsWith(File.separator)) {
            basePath = basePath + File.separator;
        }
        return basePath;
    }
}
